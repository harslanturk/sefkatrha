<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Advisor;
use App\Notification;
use App\Helpers\Helper;
use Auth;

class AdvisorController extends Controller
{
    public function index()
    {
        $advisor = Advisor::where('status', 1)->orderBy('created_at', 'desc')->get();
        Helper::readNotification("/admin/advisor", 6);
        return view('admin.advisor.index', ['advisors' => $advisor]);
    }
    public function save(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $advisor = new Advisor();
        $advisor->aciklama = $data['aciklama'];
        $advisor->status = 1;
        $advisor->save();
        //Notifikasyon Alanı
        Helper::notification($user->id, "Admin", $data, "2", "/admin/advisor", "0");
        return redirect()->back();
    }
    public function delete($id)
    {
        $notice = Advisor::where('id', $id)->first();
        $notice->status = 0;
        $notice->save();
        return redirect()->back();
    }
    public function read($id)
    {
        $notice = Advisor::where('id', $id)->first();
        if($notice->isread == 0)
        {
            $isread = 1;
        }
        else
        {
            $isread = 0;
        }
        $notice->isread = $isread;
        $notice->save();
        return redirect()->back();
    }
}
