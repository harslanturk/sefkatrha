<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\User;
use App\Chat;

class ChatController extends Controller
{
    public function saveMessage(Request $request)
    {
        $sender = Auth::user();
        $receiver = $request->receiver;
        $pvMessage = $request->pvMessage;

        $message = new Chat();
        $message->sender = $sender->id;
        $message->reciever = $receiver;
        $message->content = $pvMessage;
        $message->status = 0;
        $message->ipconfig = $request->ip();
        $message->save();
    }
    public function showMessage($reciever = "0"){

        $sender = Auth::user();

        //$contactList = DB::select("SELECT sender_id.id as sender_id FROM chat INNER JOIN users as sender_id ON (chat.sender = sender_id.id) INNER JOIN users as receiver_username ON (chat.reciever = receiver_username.id) WHERE chat.reciever = '$sender->id' GROUP BY chat.sender ORDER BY chat.created_at");
        $contactList = User::where('status', 1)->get();
        /*echo "<pre>";
        print_r($contactList);
        die();*/
        if($reciever=="0")
        {
            return view('admin.message.index', ['status' => '0', 'contactList' => $contactList]);
        }
        else
        {

            $recieverInform = User::where('id', $reciever)->first();
            /*$checkMessage = Message::where('sender', $sender->id)->where('receiver', $recieverInform->id)->andWhere()->get();*/
            if($recieverInform)
            {
                $checkMessage = DB::select("SELECT * FROM chat WHERE (sender = '$sender->id' and reciever = '$recieverInform->id') or (sender = '$recieverInform->id' and reciever = '$sender->id') ORDER BY created_at");
            }
            /*echo "<pre>";
              print_r($checkMessage);
              die();*/


            if($checkMessage)
            {
                //die("burdamı");
                /*echo $recieverInform;
                echo "<pre>";
                print_r($checkMessage);
                die();
                die("buraya gelemezsin");*/
                $message = DB::select("SELECT chat.*, sender_username.name as sender_username, receiver_username.name as receiver_username FROM chat INNER JOIN users as sender_username ON (chat.sender = sender_username.id) INNER JOIN users as receiver_username ON (chat.reciever = receiver_username.id)WHERE (sender = '$sender->id' and reciever = '$recieverInform->id') or (sender = '$recieverInform->id' and reciever = '$sender->id') ORDER BY chat.created_at");
                $messageLast = DB::select("SELECT chat.*, sender_username.name as sender_username, receiver_username.name as receiver_username FROM chat INNER JOIN users as sender_username ON (chat.sender = sender_username.id) INNER JOIN users as receiver_username ON (chat.reciever = receiver_username.id)WHERE (sender = '$sender->id' and reciever = '$recieverInform->id') or (sender = '$recieverInform->id' and reciever = '$sender->id') ORDER BY chat.created_at DESC LIMIT 0,1");
                $receiverUsername = User::where('id', $reciever)->first();
                /*if($sender->id==$reciever)
                {*/
                    Chat::where('sender', '=', $sender->id)
                        ->orwhere('sender', '=', $recieverInform->id)
                        ->where('reciever', '=', $recieverInform->id)
                        ->orWhere('reciever', '=', $sender->id)
                        ->where('status', '=', '0')
                        ->update(['status' => '1']);
                //}
                /*echo "<pre>";
                print_r($messageLast);
                die();*/
                return view('admin.message.index', ['oldMessage' => $message,'lastMessage' => $messageLast, 'status' => '1', 'receiver' => $recieverInform->id, 'receiverUsername' => $receiverUsername->name, 'contactList' => $contactList]);
            }
            else
            {
                $receiverUsername = User::where('id', $reciever)->first();
                return view('admin.message.index', ['status' => '2', 'receiver' => $recieverInform->id, 'receiverUsername' => $receiverUsername->name, 'contactList' => $contactList]);
            }
        }
    }
    public function loadMessage($receiver = "0", $lastid = "0")
    {
        //$lastid = $lastid +1;
        $sender = Auth::user();
        /*$msgSearch = Message::where('secret', $secret)->where('sender', $user->id)->first();*/
        $message = DB::select("SELECT chat.*, sender_username.name as sender_username, receiver_username.name as receiver_username FROM chat INNER JOIN users as sender_username ON (chat.sender = sender_username.id) INNER JOIN users as receiver_username ON (chat.reciever = receiver_username.id)WHERE (chat.id > '$lastid') and ((sender = '$sender->id' and reciever = '$receiver') or (sender = '$receiver' and reciever = '$sender->id')) ORDER BY chat.created_at");
        /*echo "<pre>";
        print_r($message);
        die();*/
        if(empty($message))
        {
            $status = 2;
            return view('admin.message.callmessage', ['status' => $status, 'sender' => $sender, 'receiver' => $receiver]);
        }
        else
        {
            $status = 1;
            return view('admin.message.callmessage', ['status' => $status, 'oldMessage' => $message, 'receiver' => $receiver]);
        }
    }
    public function loadMessageLast(){
        $user = Auth::user();
        $message = DB::select("SELECT message.*, sender_username.username as sender_username, receiver_username.username as receiver_username FROM message INNER JOIN users as sender_username ON (message.sender = sender_username.id) INNER JOIN users as receiver_username ON (message.receiver = receiver_username.id) WHERE (receiver = '$user->id') and status = 0 GROUP BY message.sender ORDER BY message.created_at LIMIT 0,5");
        $msgCount = Message::where('receiver', $user->id)->where('status', '0')->groupBy('receiver')->count();
        /*echo "<pre>";
        print_r($msgCount);
        die();*/
        return view('message.msglist',['message' => $message, 'msgCount' => $msgCount, 'user' => $user]);
    }
}
