<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class DeleteController extends Controller
{
    public function simpleDelete($tablo,$id)
    {
        if($tablo=="ogrenciislemleri")
        {
            $tablo="ogrenci_islemleri";
        }
        elseif($tablo=="engeltipleri")
        {
            $tablo="engel_tipleri";
        }
        elseif($tablo=="studenvisitstatus")
        {
            $tablo="student_visit_status";
        }
        elseif($tablo=="servis")
        {
            $tablo="servis";
        }
        elseif($tablo=="sinif")
        {
            $tablo="sinif";
        }
        elseif($tablo=="grup")
        {
            $tablo="plan_group";
        }
        DB::table($tablo)->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
