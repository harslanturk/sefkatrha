<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AuthGroup;
use App\Modules;
use App\Authorization;
use App\Helpers\helper;
use Auth;
use DB;

class AuthorizationController extends Controller
{
    public function index()
    {
      $auth_group = AuthGroup::where('status','1')->get();
      $column = Modules::where('status','1')->orderBy('id','asc')->get();

      return view('admin.authorization.index',[
        'auth_groups' => $auth_group,
        'columns' => $column
      ]);
    }

    public function createAuth(Request $request)
    {
      $data = $request->all();
      //  echo '<pre>';
      // print_r($data);
      // die();
      $sil = Authorization::where('group_id',$data['group_id'])->first();

      if ($sil) {
        Authorization::where('group_id','=',$data['group_id'])->delete();
      }

      foreach ($data['read'] as $key=>$dat) {
        $auth = new Authorization();
        $auth->modul_id = $key;
        $auth->group_id = $data['group_id'];
        $auth->read = $data['read'][$key];
        $auth->update = $data['update'][$key];
        $auth->delete = $data['delete'][$key];
        $auth->add = $data['add'][$key];
        $auth->save();
      }
      return redirect()->back();
    }

    public function getAuth(Request $request,$id)
    {
      $modules = Modules::where('status','1')->orderBy('id','asc')->get();
      $data = $request->all();
      $group = AuthGroup::where('id',$id)->first();
      $yetkiler = DB::table('emc_modules')
                   ->leftJoin(DB::raw('(SELECT * FROM authorization WHERE group_id = '.$id.') authorization'),'emc_modules.id','=','authorization.modul_id')
                   ->orderBy('authorization.modul_id','asc')
                   ->select('authorization.*','emc_modules.name','emc_modules.id as m_id')
                   ->get();

                    // echo '<pre>';
                    // print_r($yetkiler);
                    // die();
      return view('admin.authorization.getAuthorization',[
        'yetkilers' => $yetkiler,
        'modules' => $modules,
        'group_id' => $id,
        'group' => $group
      ]);
    }
    public function deleteAuthGroup($id)
    {
      AuthGroup::where('id',$id)->update(['status'=>0]);
      return redirect()->back();
    }
    public function createAuthGroup(Request $request)
    {
      $data = $request->all();
      $data['status'] = 1;
      AuthGroup::create($data);

      return redirect()->back();
    }
}
