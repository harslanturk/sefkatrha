<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ogrenci;
use App\OgrenciResim;
use App\EngelTipleri;
use DB;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;

class GuestStudentController extends Controller
{
    public function ogrenciEkle()
    {
      if (helper::authControl('misafir-ogrenci-ekle','add')) {
        $engelTipleri = EngelTipleri::all();
        return view('admin.guest-ogrenci.ogrenci_ekle', ['engelTipleri' => $engelTipleri]);
      }
            return redirect()->action('Admin\HomeController@index');

    }
    public function addOgrenci()
    {
        $ogrenci = new Ogrenci();
        $ogrenci->ogrenci_no = $_POST['inputOgrenciNo'];
        $ogrenci->tcno = $_POST['inputTcNo'];
        $ogrenci->engel_tipi = $_POST['inputEngelTipi'];
        $ogrenci->ad = $_POST['inputAdi'];
        $ogrenci->soyad = $_POST['inputSoyadi'];
        $ogrenci->cinsiyet = $_POST['inputCinsiyeti'];
        $ogrenci->dogum_tarihi = $_POST['inputDogumTarihi'];
        $ogrenci->dogum_yeri = $_POST['inputDogumYeri'];
        $ogrenci->okul = $_POST['inputOkulunAdi'];
        $ogrenci->baba = $_POST['inputBabaAdi'];
        $ogrenci->baba_tel = $_POST['inputBabaTel'];
        $ogrenci->kayit_tarihi = $_POST['inputKayitTarihi'];
        $ogrenci->anne = $_POST['inputAnneAdi'];
        $ogrenci->anne_tel = $_POST['inputAnneTel'];
        $ogrenci->ayrilis_tarihi = $_POST['inputAyrilisTarihi'];
        $ogrenci->egitim = $_POST['inputEgitimSekli'];
        $ogrenci->diger_tel = $_POST['inputDigerTel'];
        $ogrenci->il = $_POST['inputIl'];
        $ogrenci->ilce = $_POST['inputIlce'];
        $ogrenci->adres = $_POST['inputAdres'];
        $ogrenci->status = 2;
        $ogrenci->save();
        Flash::message('Öğrenci başarılı bir şekilde eklendi.','success');

        return redirect()->action('Admin\GuestStudentController@listOgrenci');
    }
    public function listOgrenci()
    {
        if(helper::authControl('tum-misafir-ogrenciler','read')){
          $ogrenci = Ogrenci::where('status', 2)->get();
          return view('admin.guest-ogrenci.ogrenci_list',['ogrenci' => $ogrenci]);
        }
          return redirect()->action('Admin\HomeController@index');

    }
    public function saveOgrenci(Request $request)
    {
        $ogrenci = $request->all();
        $ogrencitopla = Ogrenci::find($ogrenci['id'])->first();
        $ogrencitopla->update($request->all());
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci['id'])->first();
        $engelTipleri = EngelTipleri::all();
        /*Ogrenci::update($ogrenci);echo "<pre>";
        print_r($ogrenci);
        die();
        $ogrencitopla = Ogrenci::find($ogrenci['id'])->first();*/
        return view('admin.guest-ogrenci.ogrenci_duzenle', ['ogrenci' => $ogrencitopla, 'ogrenciresim' => $ogrenciResim, 'engelTipleri' => $engelTipleri]);
    }
    public function saveOgrenciResim(Request $request)
    {
        $ogrenciResim = $request->all();
        $ogrenci_id = $request->input('ogrenci_id');
        /*$ogrenci_resim = $request->file('resim');
        $ogrenci_resim = $request->file('resim');*/

        $path = base_path() . '/public/uploads/'.$ogrenci_id;
        /*echo "<pre>";
        echo "SELAM";
        print_r($ogrenci_resim);
        die();*/
        $imageTempName = $request->file('resim')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('resim')->getClientOriginalName();

        $request->file('resim')->move($path , $imageName);
        $newresim = '/uploads/'.$ogrenci_id."/".$imageName;
        $data= array('ogrenci_id'=> $ogrenci_id, 'resim' => $newresim);
        $ogrencitopla = OgrenciResim::updateOrCreate(['ogrenci_id' => $ogrenci_id],$data);

        return redirect()->back();
    }
    public function detayOgrenci($id)
    {
        $ogrenci = Ogrenci::where('id', $id)->first();
        //$islem_detay = IslemDetay::where('status', 1);
        $islem_detay = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->join('users', 'islem_detay.alici_id', '=', 'users.id')
            ->where('islem_detay.ogrenci_id', $id)
            ->orderBy('created_at', 'desc')
            ->select('islem_detay.*','ogrenci_islemleri.islem as konu_islem','users.name as sorumlu')
            ->get();
        $islem_detay_sp = DB::table('islem_detay')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->where('islem_detay.ogrenci_id', $id)
            ->where('islem_detay.status', 1)
            ->select('ogrenci_islemleri.islem as konu_islem')
            ->get();
        /*echo "<pre>";
        print_r($islem_detay_sp);
        die();*/
        return view('admin.guest-ogrenci.ogrenci_detay',['ogrenci' => $ogrenci, 'islemDetay' => $islem_detay, 'islemDetaySp' => $islem_detay_sp]);
    }
    public function duzenleOgrenci($id="1")
    {
        $ogrenci = Ogrenci::where('id', $id)->first();
        $ogrenciResim = OgrenciResim::where('ogrenci_id', $ogrenci->id)->first();
        $engelTipleri = EngelTipleri::all();

        return view('admin.guest-ogrenci.ogrenci_duzenle',['ogrenci' => $ogrenci, 'ogrenciresim' => $ogrenciResim, 'engelTipleri' => $engelTipleri]);
    }
}
