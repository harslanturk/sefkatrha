<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notice;
use DB;
use Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Storage;


class HomeController extends Controller
{
    public function index()
    {
        $personelCount = DB::table('users')->where('status', 1)->count();
        $ogrenciCount = DB::table('ogrenci')->where('status', 1)->count();
        $messageCount = DB::table('message')->count();
        $aktifKonuCount = DB::table('islem_detay')->where('alici_id', Auth::user()->id)->where('status', 1)->count();
        $onayKonuCount = DB::table('islem_detay')->where('alici_id', Auth::user()->id)->where('status', 2)->count();
        $redKonuCount = DB::table('islem_detay')->where('alici_id', Auth::user()->id)->where('status', 3)->count();
        $notice = Notice::where('status', 1)->orderBy('id', 'DESC')->get();
        /*$islem_detay = DB::table('islem_detay_list')
            ->join(DB::raw('SELECT islem_detay_list.konu_id, MAX(islem_detay_list.created_at) created_at FROM islem_detay_list GROUP BY islem_detay_list.konu_id as t2'), 't2.konu_id', '=', 'islem_detay_list.konu_id')
            ->join('islem_detay', 'islem_detay.id', '=', 'islem_detay_list.konu_id')
            ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
            ->join('users', 'islem_detay.alici_id', '=', 'users.id')
            ->join('ogrenci', 'ogrenci.id', '=', 'islem_detay_list.ogrenci_id')
            ->where('islem_detay.status', '!=', 0)
            ->groupBy('islem_detay_list.konu_id')
            ->orderBy('islem_detay_list.id', 'desc')
            ->select('ogrenci.ad as ogrenci_ad', 'ogrenci.soyad as ogrenci_soyad',DB::raw('max(islem_detay_list.id) as id'), DB::raw('islem_detay_list.ogrenci_id'), DB::raw('islem_detay_list.konu_id'), DB::raw('islem_detay_list.gonderen_id'), DB::raw('islem_detay_list.alici_id'), DB::raw('islem_detay_list.islem'), DB::raw('islem_detay_list.aciklama'), DB::raw('islem_detay_list.created_at'),'ogrenci_islemleri.islem as konu_islem','users.name as sorumlu')
            ->take(20)
            ->get();*/
            $islem_detay = DB::select( DB::raw("SELECT t1.ogrenci_id, t1.konu_id, t1.gonderen_id, t1.alici_id, t1.islem, t1.aciklama, t1.created_at,ogrenci.ad ogrenci_ad, ogrenci.soyad ogrenci_soyad, ogrenci_islemleri.islem konu_islem, users.name sorumlu FROM islem_detay_list t1
JOIN (SELECT islem_detay_list.konu_id, MAX(islem_detay_list.created_at) created_at FROM islem_detay_list GROUP BY islem_detay_list.konu_id) t2
    ON t1.konu_id = t2.konu_id AND t1.created_at = t2.created_at
    JOIN islem_detay ON islem_detay.id = t1.konu_id
    JOIN ogrenci ON ogrenci.id = t1.ogrenci_id
    JOIN ogrenci_islemleri ON ogrenci_islemleri.id = islem_detay.konu
    JOIN users ON users.id = t1.gonderen_id
     WHERE t1.`status` != 0 ORDER BY t1.id DESC LIMIT 0,20") );
        Helper::readNotification("/", 3);
        /*$islem_detay = DB::table('islem_detay_list')
        ->join('islem_detay', 'islem_detay.id', '=', 'islem_detay_list.konu_id')
        ->join('ogrenci_islemleri', 'islem_detay.konu', '=', 'ogrenci_islemleri.id')
        ->join('users', 'islem_detay.alici_id', '=', 'users.id')
        ->join('ogrenci', 'ogrenci.id', '=', 'islem_detay_list.ogrenci_id')
        ->where('islem_detay.status', '!=', 0)
        ->orderBy('islem_detay_list.id', 'DESC')
        ->groupBy('islem_detay_list.konu_id')
        ->select('ogrenci.ad as ogrenci_ad','ogrenci.soyad as ogrenci_soyad',DB::raw('max(islem_detay_list.created_at) as created_at'),'islem_detay_list.ogrenci_id','islem_detay_list.konu_id','islem_detay_list.gonderen_id','ogrenci_islemleri.islem as konu_islem','users.name as sorumlu','islem_detay_list.islem as dene','islem_detay_list.aciklama')
        ->take(20)
        ->get();*/


        /*echo "<pre>";
        print_r($islem_detay);
        die();*/
        /*$islem_detay = DB::select('SELECT ogrenci.ad as ogrenci_ad, ogrenci.soyad as ogrenci_soyad, islem_detay_list.*, ogrenci_islemleri.islem as konu_islem, users.name as sorumlu FROM (SELECT * FROM islem_detay_list order by id DESC) AS islem_detay_list
INNER JOIN ogrenci ON ogrenci.id = islem_detay_list.ogrenci_id
INNER JOIN islem_detay ON islem_detay.id = islem_detay_list.konu_id
INNER JOIN ogrenci_islemleri ON islem_detay.konu = ogrenci_islemleri.id
INNER JOIN users ON users.id = islem_detay_list.alici_id
group by ogrenci_id
order by created_at DESC');*/

        /*$islem_detay = DB::table('islem_detay_list')
                            ->join('ogrenci','ogrenci.id','=','islem_detay_list.ogrenci_id')
                            ->join('islem_detay','islem_detay.id','=','islem_detay_list.konu_id')
                            ->join('ogrenci_islemleri','ogrenci_islemleri.id','=','islem_detay.konu')
                            ->join('users','users.id','=','islem_detay_list.alici_id')
                            ->select('ogrenci.ad as ogrenci_ad','ogrenci.soyad as ogrenci_soyad','islem_detay_list.*', 'ogrenci_islemleri.islem as konu_islem','users.name as sorumlu')
                            ->groupBy('islem_detay.ogrenci_id')
                            ->orderBy('islem_detay_list.id','asc')->take(20)->get();*/


        return view('admin.home',[
            'ogrenciCount' => $ogrenciCount,
            'personelCount' => $personelCount,
            'messageCount' => $messageCount,
            'aktifKonuCount' => $aktifKonuCount,
            'onayKonuCount' => $onayKonuCount,
            'redKonuCount' => $redKonuCount,
            'notices' => $notice,
            'islemDetay' => $islem_detay
        ]);
    }
    public function test()
    {
        $a = "Ahmet vurduruyor.";
        $b = $a;
        echo $a."-------------------".$b;
        die();

        /*for ($i=0; $i < 5; $i++) {
            $dene['hasan'.$i] = $i;
        }
        echo "<pre>";
        print_r($dene);
        die();*/
        //$files = File::allFiles("/img/upload/1508678745/");
        //$files = Storage::put('/img/upload/1508678745', $fileContents);
        //echo "<h1>ALOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO</h1>";
        // Building directory path.
        //$directory = storage_path('img/upload/1508678745');
        
        // Will return an array of files in the directory
        // Each array element is Symfony\Component\Finder\SplFileInfo object
        //$files = Illuminate\Support\Facades\File::allFiles($directory);
        
       /* foreach ($files as $file) {
            $contents = $file->getContents();
        }
        /*echo "<pre>";
        print_r($files);
        die();*/
    }
}
