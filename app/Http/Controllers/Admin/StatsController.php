<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Ogrenci;
use App\IslemDetayList;
use App\IslemDetay;
use Redirect;
use App\Team;
use App\RoleUser;
use DB;
use App\AuthGroup;

class StatsController extends Controller
{
    public function index()
    {
		$year = date("Y");
    	$girl = Ogrenci::where('cinsiyet', "Kız")->where('status', 1)->where('ayrilis_tarihi', "0000-00-00")->count();
    	$boy = Ogrenci::where('cinsiyet', "Erkek")->where('status', 1)->where('ayrilis_tarihi', "0000-00-00")->count();
		$teams = AuthGroup::where('status', 1)->get();
		/* echo '<pre>';
		print_r($teams);
		die(); */
    	/***** ÖĞRENCİ KONULARI *****/
    	$ocakIslemDetay = IslemDetay::whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetay = IslemDetay::whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-31 00:00:00"])->count();
    	$martIslemDetay = IslemDetay::whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetay = IslemDetay::whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-31 00:00:00"])->count();
    	$mayisIslemDetay = IslemDetay::whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetay = IslemDetay::whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-31 00:00:00"])->count();
    	$temmuzIslemDetay = IslemDetay::whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetay = IslemDetay::whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetay = IslemDetay::whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-31 00:00:00"])->count();
    	$ekimIslemDetay = IslemDetay::whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetay = IslemDetay::whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-31 00:00:00"])->count();
    	$aralikIslemDetay = IslemDetay::whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();

    	$ocakIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-28 00:00:00"])->count();
    	$martIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-30 00:00:00"])->count();
    	$mayisIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-30 00:00:00"])->count();
    	$temmuzIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-30 00:00:00"])->count();
    	$ekimIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-30 00:00:00"])->count();
    	$aralikIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();

    	$ocakIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-28 00:00:00"])->count();
    	$martIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-30 00:00:00"])->count();
    	$mayisIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-30 00:00:00"])->count();
    	$temmuzIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-30 00:00:00"])->count();
    	$ekimIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-30 00:00:00"])->count();
    	$aralikIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();
    	/***** ÖĞRENCİ KONULARI *****/

    	/***** ÖĞRENCİ SAYILARI *****/
    	$ocakYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-01-01", $year."-01-31"])->count();
    	$subatYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-02-01", $year."-02-31"])->count();
    	$martYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-03-01", $year."-03-31"])->count();
    	$nisanYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-04-01", $year."-04-31"])->count();
    	$mayisYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-05-01", $year."-05-31"])->count();
    	$haziranYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-06-01", $year."-06-31"])->count();
    	$temmuzYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-07-01", $year."-07-31"])->count();
    	$agustosYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-08-01", $year."-08-31"])->count();
    	$eylulYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-09-01", $year."-09-31"])->count();
    	$ekimYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-10-01", $year."-10-31"])->count();
    	$kasimYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-11-01", $year."-11-31"])->count();
    	$aralikYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-12-01", $year."-12-31"])->count();

    	$ocakYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-01-01", $year."-01-31"])->count();
    	$subatYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-02-01", $year."-02-31"])->count();
    	$martYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-03-01", $year."-03-31"])->count();
    	$nisanYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-04-01", $year."-04-31"])->count();
    	$mayisYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-05-01", $year."-05-31"])->count();
    	$haziranYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-06-01", $year."-06-31"])->count();
    	$temmuzYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-07-01", $year."-07-31"])->count();
    	$agustosYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-08-01", $year."-08-31"])->count();
    	$eylulYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-09-01", $year."-09-31"])->count();
    	$ekimYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-10-01", $year."-10-31"])->count();
    	$kasimYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-11-01", $year."-11-31"])->count();
    	$aralikYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-12-01", $year."-12-31"])->count();
    	/****** ÖĞRENCİ SAYILARI ****/

        /***** AYRILAN ÖĞRENCİ SAYILARI *****/
        $ocakAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-01-01", $year."-01-31"])->count();
        $subatAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-02-01", $year."-02-31"])->count();
        $martAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-03-01", $year."-03-31"])->count();
        $nisanAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-04-01", $year."-04-31"])->count();
        $mayisAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-05-01", $year."-05-31"])->count();
        $haziranAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-06-01", $year."-06-31"])->count();
        $temmuzAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-07-01", $year."-07-31"])->count();
        $agustosAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-08-01", $year."-08-31"])->count();
        $eylulAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-09-01", $year."-09-31"])->count();
        $ekimAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-10-01", $year."-10-31"])->count();
        $kasimAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-11-01", $year."-11-31"])->count();
        $aralikAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-12-01", $year."-12-31"])->count();

        $ocakAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-01-01", $year."-01-31"])->count();
        $subatAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-02-01", $year."-02-31"])->count();
        $martAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-03-01", $year."-03-31"])->count();
        $nisanAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-04-01", $year."-04-31"])->count();
        $mayisAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-05-01", $year."-05-31"])->count();
        $haziranAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-06-01", $year."-06-31"])->count();
        $temmuzAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-07-01", $year."-07-31"])->count();
        $agustosAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-08-01", $year."-08-31"])->count();
        $eylulAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-09-01", $year."-09-31"])->count();
        $ekimAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-10-01", $year."-10-31"])->count();
        $kasimAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-11-01", $year."-11-31"])->count();
        $aralikAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-12-01", $year."-12-31"])->count();
        /****** AYRILAN ÖĞRENCİ SAYILARI ****/

        return view('admin.stats.index', ['girl' => $girl, 'boy' => $boy, 'ocakIslemDetay' => $ocakIslemDetay, 'subatIslemDetay' => $subatIslemDetay, 'martIslemDetay' => $martIslemDetay, 'nisanIslemDetay' => $nisanIslemDetay, 'mayisIslemDetay' => $mayisIslemDetay, 'haziranIslemDetay' => $haziranIslemDetay, 'temmuzIslemDetay' => $temmuzIslemDetay, 'agustosIslemDetay' => $agustosIslemDetay, 'eylulIslemDetay' => $eylulIslemDetay, 'ekimIslemDetay' => $ekimIslemDetay, 'kasimIslemDetay' => $kasimIslemDetay, 'aralikIslemDetay' => $aralikIslemDetay, 'year' => $year, 'ocakIslemDetayRed' => $ocakIslemDetayRed, 'subatIslemDetayRed' => $subatIslemDetayRed, 'martIslemDetayRed' => $martIslemDetayRed, 'nisanIslemDetayRed' => $nisanIslemDetayRed, 'mayisIslemDetayRed' => $mayisIslemDetayRed, 'haziranIslemDetayRed' => $haziranIslemDetayRed, 'temmuzIslemDetayRed' => $temmuzIslemDetayRed, 'agustosIslemDetayRed' => $agustosIslemDetayRed, 'eylulIslemDetayRed' => $eylulIslemDetayRed, 'ekimIslemDetayRed' => $ekimIslemDetayRed, 'kasimIslemDetayRed' => $kasimIslemDetayRed, 'aralikIslemDetayRed' => $aralikIslemDetayRed, 'ocakIslemDetayOnay' => $ocakIslemDetayOnay, 'subatIslemDetayOnay' => $subatIslemDetayOnay, 'martIslemDetayOnay' => $martIslemDetayOnay, 'nisanIslemDetayOnay' => $nisanIslemDetayOnay, 'mayisIslemDetayOnay' => $mayisIslemDetayOnay, 'haziranIslemDetayOnay' => $haziranIslemDetayOnay, 'temmuzIslemDetayOnay' => $temmuzIslemDetayOnay, 'agustosIslemDetayOnay' => $agustosIslemDetayOnay, 'eylulIslemDetayOnay' => $eylulIslemDetayOnay, 'ekimIslemDetayOnay' => $ekimIslemDetayOnay, 'kasimIslemDetayOnay' => $kasimIslemDetayOnay, 'aralikIslemDetayOnay' => $aralikIslemDetayOnay, 'teams' => $teams, 'ocakYeniOgrenciErkek' => $ocakYeniOgrenciErkek, 'subatYeniOgrenciErkek' => $subatYeniOgrenciErkek, 'martYeniOgrenciErkek' => $martYeniOgrenciErkek, 'nisanYeniOgrenciErkek' => $nisanYeniOgrenciErkek, 'mayisYeniOgrenciErkek' => $mayisYeniOgrenciErkek, 'haziranYeniOgrenciErkek' => $haziranYeniOgrenciErkek, 'temmuzYeniOgrenciErkek' => $temmuzYeniOgrenciErkek, 'agustosYeniOgrenciErkek' => $agustosYeniOgrenciErkek, 'eylulYeniOgrenciErkek' => $eylulYeniOgrenciErkek, 'ekimYeniOgrenciErkek' => $ekimYeniOgrenciErkek, 'kasimYeniOgrenciErkek' => $kasimYeniOgrenciErkek, 'aralikYeniOgrenciErkek' => $aralikYeniOgrenciErkek, 'ocakYeniOgrenciKız' => $ocakYeniOgrenciKız, 'subatYeniOgrenciKız' => $subatYeniOgrenciKız, 'martYeniOgrenciKız' => $martYeniOgrenciKız, 'nisanYeniOgrenciKız' => $nisanYeniOgrenciKız, 'mayisYeniOgrenciKız' => $mayisYeniOgrenciKız, 'haziranYeniOgrenciKız' => $haziranYeniOgrenciKız, 'temmuzYeniOgrenciKız' => $temmuzYeniOgrenciKız, 'agustosYeniOgrenciKız' => $agustosYeniOgrenciKız, 'eylulYeniOgrenciKız' => $eylulYeniOgrenciKız, 'ekimYeniOgrenciKız' => $ekimYeniOgrenciKız, 'kasimYeniOgrenciKız' => $kasimYeniOgrenciKız, 'aralikYeniOgrenciKız' => $aralikYeniOgrenciKız, 'ocakAyrilanOgrenciErkek' => $ocakAyrilanOgrenciErkek, 'subatAyrilanOgrenciErkek' => $subatAyrilanOgrenciErkek, 'martAyrilanOgrenciErkek' => $martAyrilanOgrenciErkek, 'nisanAyrilanOgrenciErkek' => $nisanAyrilanOgrenciErkek, 'mayisAyrilanOgrenciErkek' => $mayisAyrilanOgrenciErkek, 'haziranAyrilanOgrenciErkek' => $haziranAyrilanOgrenciErkek, 'temmuzAyrilanOgrenciErkek' => $temmuzAyrilanOgrenciErkek, 'agustosAyrilanOgrenciErkek' => $agustosAyrilanOgrenciErkek, 'eylulAyrilanOgrenciErkek' => $eylulAyrilanOgrenciErkek, 'ekimAyrilanOgrenciErkek' => $ekimAyrilanOgrenciErkek, 'kasimAyrilanOgrenciErkek' => $kasimAyrilanOgrenciErkek, 'aralikAyrilanOgrenciErkek' => $aralikAyrilanOgrenciErkek, 'ocakAyrilanOgrenciKız' => $ocakAyrilanOgrenciKız, 'subatAyrilanOgrenciKız' => $subatAyrilanOgrenciKız, 'martAyrilanOgrenciKız' => $martAyrilanOgrenciKız, 'nisanAyrilanOgrenciKız' => $nisanAyrilanOgrenciKız, 'mayisAyrilanOgrenciKız' => $mayisAyrilanOgrenciKız, 'haziranAyrilanOgrenciKız' => $haziranAyrilanOgrenciKız, 'temmuzAyrilanOgrenciKız' => $temmuzAyrilanOgrenciKız, 'agustosAyrilanOgrenciKız' => $agustosAyrilanOgrenciKız, 'eylulAyrilanOgrenciKız' => $eylulAyrilanOgrenciKız, 'ekimAyrilanOgrenciKız' => $ekimAyrilanOgrenciKız, 'kasimAyrilanOgrenciKız' => $kasimAyrilanOgrenciKız, 'aralikAyrilanOgrenciKız' => $aralikAyrilanOgrenciKız]);
    }

    public function indexYear($year)
    {
    	$teams = Team::where('status', 1)->get();
		//$year = date("Y");
		/***** ÖĞRENCİ KONULARI *****/
    	$girl = Ogrenci::where('cinsiyet', "Kız")->where('status', 1)->where('ayrilis_tarihi', "0000-00-00")->count();
    	$boy = Ogrenci::where('cinsiyet', "Erkek")->where('status', 1)->where('ayrilis_tarihi', "0000-00-00")->count();
    	$ocakIslemDetay = IslemDetay::whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetay = IslemDetay::whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-31 00:00:00"])->count();
    	$martIslemDetay = IslemDetay::whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetay = IslemDetay::whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-31 00:00:00"])->count();
    	$mayisIslemDetay = IslemDetay::whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetay = IslemDetay::whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-31 00:00:00"])->count();
    	$temmuzIslemDetay = IslemDetay::whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetay = IslemDetay::whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetay = IslemDetay::whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-31 00:00:00"])->count();
    	$ekimIslemDetay = IslemDetay::whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetay = IslemDetay::whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-31 00:00:00"])->count();
    	$aralikIslemDetay = IslemDetay::whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();

    	$ocakIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-28 00:00:00"])->count();
    	$martIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-30 00:00:00"])->count();
    	$mayisIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-30 00:00:00"])->count();
    	$temmuzIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-30 00:00:00"])->count();
    	$ekimIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-30 00:00:00"])->count();
    	$aralikIslemDetayRed = IslemDetay::where('status', 0)->whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();

    	$ocakIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-01-01 00:00:00", $year."-01-31 00:00:00"])->count();
    	$subatIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-02-01 00:00:00", $year."-02-28 00:00:00"])->count();
    	$martIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-03-01 00:00:00", $year."-03-31 00:00:00"])->count();
    	$nisanIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-04-01 00:00:00", $year."-04-30 00:00:00"])->count();
    	$mayisIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-05-01 00:00:00", $year."-05-31 00:00:00"])->count();
    	$haziranIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-06-01 00:00:00", $year."-06-30 00:00:00"])->count();
    	$temmuzIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-07-01 00:00:00", $year."-07-31 00:00:00"])->count();
    	$agustosIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-08-01 00:00:00", $year."-08-31 00:00:00"])->count();
    	$eylulIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-09-01 00:00:00", $year."-09-30 00:00:00"])->count();
    	$ekimIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-10-01 00:00:00", $year."-10-31 00:00:00"])->count();
    	$kasimIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-11-01 00:00:00", $year."-11-30 00:00:00"])->count();
    	$aralikIslemDetayOnay = IslemDetay::where('status', 2)->whereBetween('created_at', [$year."-12-01 00:00:00", $year."-12-31 00:00:00"])->count();
    	/***** ÖĞRENCİ KONULARI *****/

    	/***** ÖĞRENCİ SAYILARI *****/
    	$ocakYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-01-01", $year."-01-31"])->count();
    	$subatYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-02-01", $year."-02-31"])->count();
    	$martYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-03-01", $year."-03-31"])->count();
    	$nisanYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-04-01", $year."-04-31"])->count();
    	$mayisYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-05-01", $year."-05-31"])->count();
    	$haziranYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-06-01", $year."-06-31"])->count();
    	$temmuzYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-07-01", $year."-07-31"])->count();
    	$agustosYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-08-01", $year."-08-31"])->count();
    	$eylulYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-09-01", $year."-09-31"])->count();
    	$ekimYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-10-01", $year."-10-31"])->count();
    	$kasimYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-11-01", $year."-11-31"])->count();
    	$aralikYeniOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->whereBetween('kayit_tarihi', [$year."-12-01", $year."-12-31"])->count();

    	$ocakYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-01-01", $year."-01-31"])->count();
    	$subatYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-02-01", $year."-02-31"])->count();
    	$martYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-03-01", $year."-03-31"])->count();
    	$nisanYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-04-01", $year."-04-31"])->count();
    	$mayisYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-05-01", $year."-05-31"])->count();
    	$haziranYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-06-01", $year."-06-31"])->count();
    	$temmuzYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-07-01", $year."-07-31"])->count();
    	$agustosYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-08-01", $year."-08-31"])->count();
    	$eylulYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-09-01", $year."-09-31"])->count();
    	$ekimYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-10-01", $year."-10-31"])->count();
    	$kasimYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-11-01", $year."-11-31"])->count();
    	$aralikYeniOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->whereBetween('kayit_tarihi', [$year."-12-01", $year."-12-31"])->count();
    	/****** ÖĞRENCİ SAYILARI ****/

        /***** AYRILAN ÖĞRENCİ SAYILARI *****/
        $ocakAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-01-01", $year."-01-31"])->count();
        $subatAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-02-01", $year."-02-31"])->count();
        $martAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-03-01", $year."-03-31"])->count();
        $nisanAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-04-01", $year."-04-31"])->count();
        $mayisAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-05-01", $year."-05-31"])->count();
        $haziranAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-06-01", $year."-06-31"])->count();
        $temmuzAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-07-01", $year."-07-31"])->count();
        $agustosAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-08-01", $year."-08-31"])->count();
        $eylulAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-09-01", $year."-09-31"])->count();
        $ekimAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-10-01", $year."-10-31"])->count();
        $kasimAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-11-01", $year."-11-31"])->count();
        $aralikAyrilanOgrenciErkek = Ogrenci::where('status', 1)->where('cinsiyet', "Erkek")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-12-01", $year."-12-31"])->count();

        $ocakAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-01-01", $year."-01-31"])->count();
        $subatAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-02-01", $year."-02-31"])->count();
        $martAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-03-01", $year."-03-31"])->count();
        $nisanAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-04-01", $year."-04-31"])->count();
        $mayisAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-05-01", $year."-05-31"])->count();
        $haziranAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-06-01", $year."-06-31"])->count();
        $temmuzAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-07-01", $year."-07-31"])->count();
        $agustosAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-08-01", $year."-08-31"])->count();
        $eylulAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-09-01", $year."-09-31"])->count();
        $ekimAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-10-01", $year."-10-31"])->count();
        $kasimAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-11-01", $year."-11-31"])->count();
        $aralikAyrilanOgrenciKız = Ogrenci::where('status', 1)->where('cinsiyet', "Kız")->where('ayrilis_tarihi', '!=' ,"0000-00-00")->whereBetween('ayrilis_tarihi', [$year."-12-01", $year."-12-31"])->count();
        /****** AYRILAN ÖĞRENCİ SAYILARI ****/

        return view('admin.stats.index', ['girl' => $girl, 'boy' => $boy, 'ocakIslemDetay' => $ocakIslemDetay, 'subatIslemDetay' => $subatIslemDetay, 'martIslemDetay' => $martIslemDetay, 'nisanIslemDetay' => $nisanIslemDetay, 'mayisIslemDetay' => $mayisIslemDetay, 'haziranIslemDetay' => $haziranIslemDetay, 'temmuzIslemDetay' => $temmuzIslemDetay, 'agustosIslemDetay' => $agustosIslemDetay, 'eylulIslemDetay' => $eylulIslemDetay, 'ekimIslemDetay' => $ekimIslemDetay, 'kasimIslemDetay' => $kasimIslemDetay, 'aralikIslemDetay' => $aralikIslemDetay, 'year' => $year, 'ocakIslemDetayRed' => $ocakIslemDetayRed, 'subatIslemDetayRed' => $subatIslemDetayRed, 'martIslemDetayRed' => $martIslemDetayRed, 'nisanIslemDetayRed' => $nisanIslemDetayRed, 'mayisIslemDetayRed' => $mayisIslemDetayRed, 'haziranIslemDetayRed' => $haziranIslemDetayRed, 'temmuzIslemDetayRed' => $temmuzIslemDetayRed, 'agustosIslemDetayRed' => $agustosIslemDetayRed, 'eylulIslemDetayRed' => $eylulIslemDetayRed, 'ekimIslemDetayRed' => $ekimIslemDetayRed, 'kasimIslemDetayRed' => $kasimIslemDetayRed, 'aralikIslemDetayRed' => $aralikIslemDetayRed, 'ocakIslemDetayOnay' => $ocakIslemDetayOnay, 'subatIslemDetayOnay' => $subatIslemDetayOnay, 'martIslemDetayOnay' => $martIslemDetayOnay, 'nisanIslemDetayOnay' => $nisanIslemDetayOnay, 'mayisIslemDetayOnay' => $mayisIslemDetayOnay, 'haziranIslemDetayOnay' => $haziranIslemDetayOnay, 'temmuzIslemDetayOnay' => $temmuzIslemDetayOnay, 'agustosIslemDetayOnay' => $agustosIslemDetayOnay, 'eylulIslemDetayOnay' => $eylulIslemDetayOnay, 'ekimIslemDetayOnay' => $ekimIslemDetayOnay, 'kasimIslemDetayOnay' => $kasimIslemDetayOnay, 'aralikIslemDetayOnay' => $aralikIslemDetayOnay, 'teams' => $teams, 'ocakYeniOgrenciErkek' => $ocakYeniOgrenciErkek, 'subatYeniOgrenciErkek' => $subatYeniOgrenciErkek, 'martYeniOgrenciErkek' => $martYeniOgrenciErkek, 'nisanYeniOgrenciErkek' => $nisanYeniOgrenciErkek, 'mayisYeniOgrenciErkek' => $mayisYeniOgrenciErkek, 'haziranYeniOgrenciErkek' => $haziranYeniOgrenciErkek, 'temmuzYeniOgrenciErkek' => $temmuzYeniOgrenciErkek, 'agustosYeniOgrenciErkek' => $agustosYeniOgrenciErkek, 'eylulYeniOgrenciErkek' => $eylulYeniOgrenciErkek, 'ekimYeniOgrenciErkek' => $ekimYeniOgrenciErkek, 'kasimYeniOgrenciErkek' => $kasimYeniOgrenciErkek, 'aralikYeniOgrenciErkek' => $aralikYeniOgrenciErkek, 'ocakYeniOgrenciKız' => $ocakYeniOgrenciKız, 'subatYeniOgrenciKız' => $subatYeniOgrenciKız, 'martYeniOgrenciKız' => $martYeniOgrenciKız, 'nisanYeniOgrenciKız' => $nisanYeniOgrenciKız, 'mayisYeniOgrenciKız' => $mayisYeniOgrenciKız, 'haziranYeniOgrenciKız' => $haziranYeniOgrenciKız, 'temmuzYeniOgrenciKız' => $temmuzYeniOgrenciKız, 'agustosYeniOgrenciKız' => $agustosYeniOgrenciKız, 'eylulYeniOgrenciKız' => $eylulYeniOgrenciKız, 'ekimYeniOgrenciKız' => $ekimYeniOgrenciKız, 'kasimYeniOgrenciKız' => $kasimYeniOgrenciKız, 'aralikYeniOgrenciKız' => $aralikYeniOgrenciKız, 'ocakAyrilanOgrenciErkek' => $ocakAyrilanOgrenciErkek, 'subatAyrilanOgrenciErkek' => $subatAyrilanOgrenciErkek, 'martAyrilanOgrenciErkek' => $martAyrilanOgrenciErkek, 'nisanAyrilanOgrenciErkek' => $nisanAyrilanOgrenciErkek, 'mayisAyrilanOgrenciErkek' => $mayisAyrilanOgrenciErkek, 'haziranAyrilanOgrenciErkek' => $haziranAyrilanOgrenciErkek, 'temmuzAyrilanOgrenciErkek' => $temmuzAyrilanOgrenciErkek, 'agustosAyrilanOgrenciErkek' => $agustosAyrilanOgrenciErkek, 'eylulAyrilanOgrenciErkek' => $eylulAyrilanOgrenciErkek, 'ekimAyrilanOgrenciErkek' => $ekimAyrilanOgrenciErkek, 'kasimAyrilanOgrenciErkek' => $kasimAyrilanOgrenciErkek, 'aralikAyrilanOgrenciErkek' => $aralikAyrilanOgrenciErkek, 'ocakAyrilanOgrenciKız' => $ocakAyrilanOgrenciKız, 'subatAyrilanOgrenciKız' => $subatAyrilanOgrenciKız, 'martAyrilanOgrenciKız' => $martAyrilanOgrenciKız, 'nisanAyrilanOgrenciKız' => $nisanAyrilanOgrenciKız, 'mayisAyrilanOgrenciKız' => $mayisAyrilanOgrenciKız, 'haziranAyrilanOgrenciKız' => $haziranAyrilanOgrenciKız, 'temmuzAyrilanOgrenciKız' => $temmuzAyrilanOgrenciKız, 'agustosAyrilanOgrenciKız' => $agustosAyrilanOgrenciKız, 'eylulAyrilanOgrenciKız' => $eylulAyrilanOgrenciKız, 'ekimAyrilanOgrenciKız' => $ekimAyrilanOgrenciKız, 'kasimAyrilanOgrenciKız' => $kasimAyrilanOgrenciKız, 'aralikAyrilanOgrenciKız' => $aralikAyrilanOgrenciKız]);
    }

    public function postYear(Request $request)
    {
		$year = $request->input('year');
    	return Redirect::to("/admin/stats/".$year);
    }
    public function update()
    {
        return view('admin.stats.update');
    }
}
