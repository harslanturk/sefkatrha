<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Planlama;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use DB;
use Session;
use App\User;
use App\Ogrenci;
use App\Plan;
use App\PlanGroup;
use App\Sinif;
use App\Servis;
use App\ProgramList;

class PlanlamaController extends Controller
{
  public function api()
  {
      $events = DB::table('planlama')
                  ->join('users','users.id','=','planlama.personel_id')
                  ->join('ogrenci','ogrenci.id','=','planlama.ogrenci_id')
                  ->where('planlama.status', 1)
                  ->select('planlama.id as id','planlama.ogrenci_id','planlama.personel_id','planlama.user_id','planlama.sinif','planlama.servis','planlama.start_time as start','planlama.end_time as end',DB::raw('CONCAT(ogrenci.ad," ",ogrenci.soyad) as title'),'users.color')
                  ->get();
     /* echo '<pre>';
              print_r($events);
              die();*/
      return $events;
  }
  public function index()
  {
      $now = Carbon::now();
      $user_id = Auth::user()->id;
      $calendars = DB::table('planlama')
                  ->join('users','users.id','=','planlama.personel_id')
                  ->join('ogrenci','ogrenci.id','=','planlama.ogrenci_id')
                  ->where('end_time','>=',$now)
                  ->where('planlama.status', 1)
                  ->select('planlama.id as id','planlama.ogrenci_id','planlama.personel_id','planlama.user_id','planlama.sinif','planlama.servis','planlama.start_time','planlama.end_time',DB::raw('CONCAT(ogrenci.ad," ",ogrenci.soyad) as title'),'planlama.color')
                  ->orderby('start_time','asc')
                  ->get();
      // echo '<pre>';
      // print_r($calendars);
      // die();
      return view('admin.planlama.index',['calendars'=>$calendars]);
  }
  public function createCalendarModalShow()
  {
    $personel = User::where('status',1)->orderBy('name','asc')->get();
    $ogrenci = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
      return view('admin.planlama.createPlanlamaModal',[
        'personel' => $personel,
        'ogrenci' => $ogrenci
      ]);
  }
  public function calendarModalSave(Request $request)
  {
    $data = $request->all();
    $data['start_time'] = $data['start_date'].' '.$data['start_time'];
    $data['end_time'] = $data['end_date'].' '.$data['end_time'];
    $data['status'] = 1;

    Planlama::create($data);

    return redirect()->back();
    // echo '<pre>';
    // print_r($data);
    // die();
  }
  public function updateEventDrop(Request $request)
  {
    $data = $request->all();
    $sonuc = Planlama::where('id',$data['id'])->update($data);
    if ($sonuc) {
    return 1;
    }
    return 0;
  }

  public function editModal(Request $request)
  {
    $data = $request->all();
      $personel = User::where('status',1)->orderBy('name','asc')->get();
      $ogrenci = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
    $events = Planlama::where('id',$data['event_id'])->first();

    return view('admin.planlama.editModal',[
      'events' => $events,
        'personel' => $personel,
        'ogrenci' => $ogrenci
    ]);
  }

  public function update(Request $request,$id)
  {
    $data = $request->except(['_token']);
    $data['start_time'] = $data['start_date'].' '.$data['start_time'];
    $data['end_time'] = $data['end_date'].' '.$data['end_time'];
    unset($data['start_date']);
    unset($data['end_date']);
    // echo '<pre>';
    // print_r($data);
    // die();
    Planlama::where('id',$id)->update($data);
    return redirect()->back();
  }
  public function delete($id)
  {
    Planlama::where('id',$id)->update(['status' => 0]);
    return redirect()->back();
  }

  public function index2()
  {
    $plans = Plan::where('status',1)->get();
    $grups = PlanGroup::where('status',1)->get();

    return view('admin.planlama.index2',[
      'plans' => $plans,
      'grups' => $grups
    ]);
  }

  public function createProgramModal(Request $request)
  {
    return view('admin.planlama.programModalAdd');
  }

  public function saveProgramModal(Request $request)
  {
    $data = $request->all();
    $data['hafta'] = $data['start'].' '.$data['end'];

    $varmi = Plan::where('hafta',$data['hafta'])->first();
    if ($varmi) {

    Session::flash('error', 'Aynı Hafta Haydı Bulunmaktadır. Lütfen Seçiminizi Düzeltiniz.');
    return redirect()->back();
    }else {
      $data['status'] = 1;
      $data['plan_list_id'] = '{}';
      Plan::create($data);
      return redirect()->back();
    }


  }
  public function showProgram($id)
  {
    $sinifs = Sinif::where('status',1)->get();
    $servis = Servis::where('status',1)->get();
    $plans = Plan::where('status',1)->get();
    $grups = PlanGroup::where('status',1)->get();
    $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();

    return view('admin.planlama.show',[
      'plans' => $plans,
      'grups' => $grups,
      'sinifs' => $sinifs,
      'servis' => $servis,
      'ogrencis' => $ogrencis,
      'id' => $id
    ]);
  }

  public function createProgram(Request $request)
  {
    $data = $request->all();
    // echo '<pre>';
    // print_r($data);
    // die();
    if (!empty($data['grup_no'])) {
      ProgramList::where('grup_no',$data['grup_no'])->delete();
    }

    // foreach ($data['ogrenci0'] as $key => $dat) {
    //
    //   if ($key % 2 == 0) {
    //   echo '1-'.$key.'- a'.$data['ogrenci0'][$key].'<br>';
    //     echo '2-'.$key.'- a'.$data['ogrenci1'][$key].'<br>';
    //       echo '3-'.$key.'- a'.$data['ogrenci2'][$key].'<br>';
    //   }else {

    //     echo '1-'.$key.'-b'.$data['ogrenci0'][$key].'<br>';
    //       echo '2-'.$key.'-b'.$data['ogrenci1'][$key].'<br>';
    //         echo '3-'.$key.'-b'.$data['ogrenci2'][$key].'<br>';
    //   }
    //
    // }



        for ($i=0; $i < $data['grup_count']; $i++) {
          foreach ($data['sinif'.$i] as $key => $value) {
            $veri = explode('/',$value);
            $sinif = $veri[0];
            $gun = $veri[1];
            // echo "Grup ".($i+1)."Sınıf ".$sinif." Gun ".$gun." ogrenci-id-A ".$data['ogrencia'.$i][$key]."<br>";
            // echo "Grup ".($i+1)."Sınıf ".$sinif." Gun ".$gun." ogrenci-id-B ".$data['ogrencib'.$i][$key]."<br>";
            //A'lar
            $plan_list = new ProgramList();
            $plan_list->grup_no = $data['grup_no'];
            $plan_list->grup_id = ($i+1);
            $plan_list->sinif_id = $sinif;
            $plan_list->a_b = 'a';
            $plan_list->ogrenci_id = $data['ogrencia'.$i][$key];
            $plan_list->servis_id = $data['servisa'.$i][$key];
            $plan_list->calisma_gun = $gun;
            $plan_list->status = 1;
            $plan_list->save();
            //B'ler
            $plan_list = new ProgramList();
            $plan_list->grup_no = $data['grup_no'];
            $plan_list->grup_id = ($i+1);
            $plan_list->sinif_id = $sinif;
            $plan_list->a_b = 'b';
            $plan_list->ogrenci_id = $data['ogrencib'.$i][$key];
            $plan_list->servis_id = $data['servisb'.$i][$key];
            $plan_list->calisma_gun = $gun;
            $plan_list->status = 1;
            $plan_list->save();

          }
        }
        return redirect()->back();
  }
  public function editProgram($id)
  {
    //$programs = ProgramList::where('grup_no',$id)->get();
    $servis = Servis::where('status',1)->get();
    $sinifs = Sinif::where('status',1)->get();
    $plans = Plan::where('status',1)->get();
    $grups = PlanGroup::where('status',1)->get();
    $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();

    $plan = ProgramList::where('grup_no', 13)
                        ->where('grup_id', 1)
                        ->orderBy('sinif_id')
                        ->orderBy('calisma_gun')
                        ->orderBy('a_b')
                        ->get();
    $tut = "";
    $bas = 0;
    $son = 0;
    $gun_tut = 0;
    foreach($plan as $key => $val)
    {
      if($key != 0)
      {
        if($tut == $val->sinif_id)
        {
          $newdizi[$bas][$son] = $val;
          $son++;
        } 
        else{
          $tut = $val->sinif_id;
          $bas++;
          $son = 0;
          $newdizi[$bas][$son] = $val;
          $son++;
        }
      }
      else{
        $tut = $val->sinif_id;
        $gun_tut = $val->calisma_gun;
        $newdizi[$bas][$son] = $val;
        $son++;
      }
    }
    $i = 0;
    /*foreach($sinifs as $anahtar => $sinif){
      $i = $anahtar;
      echo $newdizi[$i][0]->ogrenci_id;
    }*/

    /*echo "<pre>";
    print_r($newdizi);
    die();*/

    return view('admin.planlama.update',[
      //'programs' => $programs,
      'plan' => $newdizi,
      'plans' => $plans,
      'grups' => $grups,
      'sinifs' => $sinifs,
      'ogrencis' => $ogrencis,
      'id' => $id,
      'servis' => $servis,

    ]);
  }

  public function ogrenciModal(Request $request)
  {
    $data = $request->all();
    $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
    $servis = Servis::where('status',1)->get();
    $plan = ProgramList::where('id',$data['ogrenci'])->first();

    return view('admin.planlama.ogrenciModal',[
      'ogrencis' => $ogrencis,
      'servis' => $servis,
      'plan' => $plan
    ]);
  }

  public function ogrenciModalSave(Request $request)
  {
   $data = $request->all();
    // echo '<pre>';
    // print_r($data);
    // die();
    unset($data['_token']);
    ProgramList::where('id',$data['id'])->update($data);
    return redirect()->back();
  }

  public function filterModal(Request $request)
  {
    $data = $request->all();
    $id = $data['plan_id'];
    $servis = Servis::where('status',1)->get();
    return view('admin.planlama.filterModal',[
    'servis' => $servis,
    'id' => $id
    ]);

  }

  public function filter(Request $request)
  {
    $data = $request->all();
    $id = $data['id'];

    //$programs = ProgramList::where('grup_no',$id)->get();
    if (!empty($data['engel_kategorisi'])) {
      $sinifs = Sinif::where('status',1)->where('name','like','%'.$data['engel_kategorisi'].'%')->get();
      $data['engel_kategorisi'] = $data['engel_kategorisi'];
    }else {
      $sinifs = Sinif::where('status',1)->get();
      $data['engel_kategorisi'] = '';
    }
    if (empty($data['servis_id'])) {
      $data['servis_id'] = '';
    }
    $plans = Plan::where('status',1)->get();
    $grups = PlanGroup::where('status',1)->get();
    $ogrencis = Ogrenci::where('status',1)->where('ayrilis_tarihi','=','0000-00-00')->orderBy('ad','asc')->get();
    // echo '<pre>';
    // print_r($data);
    // die();
    return view('admin.planlama.filter',[
      //'programs' => $programs,
      'plans' => $plans,
      'grups' => $grups,
      'sinifs' => $sinifs,
      'ogrencis' => $ogrencis,
      'id' => $id,
      'servis_id' => $data['servis_id'],
      'engel_kategorisi' => $data['engel_kategorisi']
    ]);
  }
  public function CopyModal(Request $request)
  {
    $data = $request->all();

    return view('admin.planlama.copyModal',[
      'id' => $data['plan_id']
    ]);
  }
  public function copySave(Request $request)
  {
    $data = $request->all();

    $data['hafta'] = $data['start'].' '.$data['end'];
    $data['status'] = 1;
    $data['plan_list_id'] = '{}';
    $plan = Plan::create($data);

      $plan_lists = ProgramList::where('grup_no',$data['id'])->get();
      // echo '<pre>';
      // print_r($plan_lists);
      // die();
      if ($plan_lists) {
        foreach ($plan_lists as $key => $plan_list) {
          $copy = new ProgramList();
          $copy->grup_no = $plan->id;
          $copy->grup_id = $plan_list->grup_id;
          $copy->sinif_id = $plan_list->sinif_id;
          $copy->a_b = $plan_list->a_b;
          $copy->ogrenci_id = $plan_list->ogrenci_id;
          $copy->servis_id  = $plan_list->servis_id;
          $copy->calisma_gun = $plan_list->calisma_gun;
          $copy->status = $plan_list->status;
          $copy->save();
        }
      }
      return redirect()->back();
  }

  public function indexServis()
  {
    $plans = Plan::where('status',1)->get();
    $servis = '';
    return view('admin.yazdir.index',[
      'plans' => $plans,
      'servis' => $servis
    ]);
  }
  public function indexServisPrint($id)
  {
    $plans = Plan::where('status',1)->get();
    $servis = Servis::where('status',1)->get();
    return view('admin.yazdir.index',[
      'plans' => $plans,
      'servis' => $servis,
      'plan_id' =>$id
    ]);
  }
  public function ServisPrint($id,$servis_id)
  {
    //$programs = ProgramList::where('grup_no',$id)->get();
    $sinifs = Sinif::where('status',1)->get();
    $grups = PlanGroup::where('status',1)->get();
    $plan_lists = ProgramList::where('grup_no',$id)->where('servis_id',$servis_id)->get();

    return view('admin.yazdir.servis-print',[
      'grups'     => $grups,
      'sinifs'    => $sinifs,
      'grup_no'   => $id,
      'id'   => $id,
      'servis_id' => $servis_id,
      'plan_lists'=> $plan_lists
    ]);
  }

  public function indexSinif()
  {
    $plans = Plan::where('status',1)->get();
    $sinif = '';
    return view('admin.yazdir.index-sinif',[
      'plans' => $plans,
      'sinif' => $sinif
    ]);
  }
  public function indexSinifPrint($id)
  {
    $plans = Plan::where('status',1)->get();
    $sinif = Sinif::where('status',1)->get();
    return view('admin.yazdir.index-sinif',[
      'plans' => $plans,
      'sinif' => $sinif,
      'plan_id' =>$id
    ]);
  }

  public function SinifPrint($id,$sinif_id)
  {
    //$programs = ProgramList::where('grup_no',$id)->get();
    $sinifs = Sinif::where('status',1)->where('name','like','%'.$sinif_id.'%')->get();
    // echo '<pre>';
    // print_r($sinifs);
    // die();
    $grups = PlanGroup::where('status',1)->get();

    return view('admin.yazdir.sinif-print',[
      'grups'     => $grups,
      'sinifs'    => $sinifs,
      'grup_no'   => $id,
      'id'   => $id
    ]);
  }
}
