<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notice;
use App\NoticeFile;
use App\Helpers\Helper;
use Auth;

class NoticeController extends Controller
{
    public function create(Request $request)
    {
        $user = Auth::user();
        $all = $request->all();
        $all['isread'] = 0;
        $all['status'] = 1;
        Notice::create($all);
        Helper::notification($user->id, "0", $all, "3", "/", "1");
        return redirect()->back();
    }
    public function delete($id)
    {
        $notice = Notice::where('id', $id)->first();
        $notice->status = 0;
        $notice->save();
        return redirect()->back();
    }
    public function fileUpload(Request $request)
    {
        $notice_key = $request->input('notice_key');
        $path = base_path() . '/public/img/upload/'.$notice_key;

        $imageTempName = $request->file('file')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('file')->getClientOriginalName();

        /*echo $notice_key."-".$path."-".$imageTempName."-".$imageName;
        die();*/
        $request->file('file')->move($path , $imageName);
        $newresim = '/img/upload/'.$notice_key.'/'.$imageName;

        $noticeFile = new NoticeFile();
        $noticeFile->file_name = $imageName;
        $noticeFile->notice_key = $notice_key;
        $noticeFile->status = "1";
        $noticeFile->save();

    }
    public function getNotice(Request $request)
    {
        $data = $request->all();
        $notice = Notice::where('notice_key', $data['notice_key'])->first();

        if(empty($notice->content))
        {
            $content = "";
        }
        else
        {
            $content = $notice->content;
        }
        $fileData = "";
        $files = NoticeFile::where('notice_key', $data['notice_key'])->get();
        foreach ($files as $file)
        {
            /*$fileData = $fileData.'<div class="col-md-3"><a href="/img/upload/$file->notice_key/$file->file_name" target="_blank">$file->file_name</a></div>';*/
            $fileData = $fileData."/".$file->file_name;

            /*$parcala = explode('.', $file->file_name);
            if($parcala[1] == "doc" || $parcala[1] == "docx")
            {
                $fileIcon = "doc.png";
            }
            else if($parcala[1] == "pdf")
            {
                $fileIcon = "pdf.png";
            }*/
        }
        //$fileData = "";
        echo $notice->notice_key."/".$notice->content.$fileData;
    }
}
