<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticeFile extends Model
{
    protected $table = 'notice_file';
    protected $fillable = [
        'notice_key','file_name','status',
    ];
}
