<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageList extends Model
{
    protected $table= "message_list";
    protected $fillable = [
        'gonderen_id', 'message_id', 'alici_id', 'mesaj', 'read',
    ];
}
