<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sinif extends Model
{
    protected $table = 'sinif';
    protected $fillable = [
      'name','status',
    ];
}
