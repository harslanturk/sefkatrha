<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personel extends Model
{
    protected $table = "personel";
    protected $fillable = [
        'yetki', 'tcno', 'ad', 'soyad', 'cinsiyet', 'dogum_tarihi', 'dogum_yeri', 'il', 'ilce', 'adres', 'baba', 'anne', 'kayit_tarihi', 'ayrilis_tarihi', 'telefon', 'is_telefon', 'diger_telefon',
    ];
}
