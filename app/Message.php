<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "message";
    protected $fillable = [
        'gonderen_id', 'alici_id', 'mesaj', 'konu', 'read', 'close',
    ];
}
