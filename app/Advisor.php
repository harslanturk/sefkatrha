<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $table = "advisor";
    protected $fillable = [
        'content', 'status', 'isread',
    ];
}
