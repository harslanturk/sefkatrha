<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramList extends Model
{
    protected $table = 'plan_list';
    protected $fillable = [
      'grup_no','grup_id','sinif_id','a_b','ogrenci_id','servis_id','calisma_gun','status'
    ];

    public function ogrenci()
    {
        return $this->belongsTo('App\Ogrenci');
    }
    public function servis()
    {
        return $this->belongsTo('App\Servis');
    }
}
