<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IslemDetayList extends Model
{
    protected $table = "islem_detay_list";
    protected $fillable = [
        'ogrenci_id', 'konu_id', 'gonderen_id', 'alici_id', 'islem', 'aciklama', 'issend',
    ];
}
