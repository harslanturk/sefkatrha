<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planlama extends Model
{
    protected $table = 'planlama';
    protected $fillable = [
      'user_id','personel_id','ogrenci_id','title','sinif','servis','start_time','end_time','status','color'
    ];
}
