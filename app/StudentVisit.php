<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentVisit extends Model
{
    protected $table = 'student_visit';
    protected $fillable = [
      'status','ogrenci_id','gonderen_id','alici_id','aciklama','tarih','gizli'
    ];
}
