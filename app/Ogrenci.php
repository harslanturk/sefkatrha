<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ogrenci extends Model
{
    protected $table = "ogrenci";
    protected $fillable = [
        'ogrenci_no', 'tcno', 'ad', 'soyad', 'cinsiyet', 'dogum_tarihi', 'dogum_yeri', 'engel_tipi', 'il', 'ilce', 'adres', 'baba', 'anne', 'kayit_tarihi', 'ayrilis_tarihi', 'okul', 'egitim', 'baba_tel', 'anne_tel', 'diger_tel','status','engel_kategorisi'
    ];
}
