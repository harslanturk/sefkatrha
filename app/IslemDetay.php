<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class IslemDetay extends Model
{
    protected $table = "islem_detay";
    protected $fillable = [
        'ogrenci_id', 'konu_id', 'gonderen_id', 'alici_id', 'islem', 'aciklama', 'status',
    ];
    public function user()
    {
        return $this->belongsTo('User');
    }
}
