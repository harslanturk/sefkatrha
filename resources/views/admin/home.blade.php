@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panel
    <small>Kontrol paneli</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard active"></i> Anasayfa</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="/admin/process/mywork">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-bookmark-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Bekleyen İşlemlerim</span>
              <span class="info-box-number">{{$aktifKonuCount}}</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </a>
      </div><!-- /.col -->
      <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="/admin/process/mywork">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-thumbs-o-up"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Onaylanan Konularım</span>
              <span class="info-box-number">{{$onayKonuCount}}</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </a>
      </div><!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="/admin/process/mywork">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-down"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Reddedilen Konularım</span>
              <span class="info-box-number">{{$redKonuCount}}</span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </a>
      </div><!-- /.col -->
      <!--<div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">New Members</span>
            <span class="info-box-number">2,000</span>
          </div>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  <!-- Main row -->
  <div class="row">
  @if(App\Helpers\helper::authControl('son-20-islem-widget','read'))
    <section class="col-lg-12 connectedSortable">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Son İşlem Yapılan 20 Konu</h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-striped">
            <tr>
              <th>#</th>
              <th>Tarih</th>
              <th>Öğrenci</th>
              <th>Konu</th>
              <th>Görevlendirilen</th>
              <th>İşlem</th>
              <th>Açıklama</th>
            </tr>
            <?php $sayac=1; ?>
            @foreach($islemDetay as $key => $value)
              <tr onclick="document.location = '/admin/process/show/{{ $value->konu_id }}';" style="cursor: pointer;">
                <td>{{$sayac}}</td>
                <td><?php $newdate = App\Helpers\Helper::DateConverter($value->created_at); echo $newdate; ?></td>
                <td>{{ $value->ogrenci_ad }} {{ $value->ogrenci_soyad }}</td>
                <td>{{ $value->konu_islem }}</td>
                <td>{{ $value->sorumlu }}</td>
                <td>{{ $value->islem }}</td>
                <td><textarea rows="1" cols="20" disabled="disabled">{{ $value->aciklama }}</textarea></td>
              </tr>
              <?php $sayac++; ?>
            @endforeach

          </table>
        </div><!-- /.box -->
      </div><!-- /.box -->
    </section><!-- /.Left col -->
  @endif
    <!-- Left col -->
    @if(App\Helpers\helper::authControl('duyurular-widget','read'))
    <section class="col-lg-7 connectedSortable">
      <!-- TO DO List -->
      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Duyurular</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <ul class="todo-list">
            @foreach($notices as $notice)
            <li>
              <!-- drag handle -->
              <span class="handle">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
              </span>
              <!-- todo text -->
              <span class="text">{{ $notice->content }}</span>
              <!-- Emphasis label -->
              @if(App\Helpers\helper::authControl('duyurular-widget','delete'))
              <button type="button" class="btn btn-danger pull-right" style="padding:2px; margin-left:10px;" onclick="deleteApprove('/admin/notice/delete/{{ $notice->id }}')"><i class="fa fa-trash"></i></button>
              @endif
              @if(App\Helpers\helper::authControl('duyurular-widget','read'))
              <button type="button" class="btn btn-primary pull-right" style="padding:2px; margin-left:10px;" onclick="noticeDetay({{ $notice->notice_key }})"><i class="fa fa-eye"></i></button>
              @endif
              <small class="label label-success pull-right" style="margin-top:5px;"><i class="fa fa-clock-o"></i> <?php echo App\Helpers\Helper::DateTimeConverter($notice->created_at);?></small>


              <!-- General tools such as edit or delete-->
            </li>
            @endforeach
          </ul>
        </div><!-- /.box-body -->
        @if(App\Helpers\helper::authControl('duyurular-widget','add'))
        <div class="box-footer clearfix no-border">
          <button class="btn btn-default pull-right" data-toggle="modal" data-target="#modalYeniNotice"><i class="fa fa-plus"></i> Duyuru Ekle</button>
        </div>
        @endif
      </div><!-- /.box -->
    </section><!-- /.Left col -->
    @endif
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    @if(App\Helpers\helper::authControl('takvim-widget','read'))
    <section class="col-lg-5 connectedSortable">
      <!-- Calendar -->
      <div class="box box-solid bg-green-gradient">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Takvim</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Olay Ekle</a></li>
                <li><a href="#">Olay Sil</a></li>
                <li class="divider"></li>
                <li><a href="#">Takvimi Görüntüle</a></li>
              </ul>
            </div>
            <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
          </div><!-- /. tools -->
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
          <!--The calendar -->
          <div id="calendar" style="width: 100%"></div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    </section><!-- right col -->
    @endif
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- Duyuru Modal -->
<?php $notice_key = strtotime(date('Y-m-d H:i:s')); ?>
<div class="modal fade" id="modalYeniNotice" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Yeni Duyuru</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputPersonel">Duyuru</label>
              <textarea class="form-control" rows="3" placeholder="Duyuru" name="content" id="content"></textarea>
            </div>
            @if(App\Helpers\helper::authControl('duyuru-eklenti','add'))
            <div class="form-group">
              <label for="exampleInputPersonel">Dosya Ekle</label>
              <form action="/admin/notice/upload" class="dropzone" id="my-awesome-dropzone">
              {!! csrf_field() !!}
              <input type="hidden" name="notice_key" id="notice_key" value="{{$notice_key}}">
              </form>
            </div>
            @endif
          </div><!-- /.box-body -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          <button type="submit" class="btn btn-primary" onclick="saveNewNotice()">Kaydet</button>
        </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modalEditNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <input type="hidden" name="notice_key" id="noticeEditKey">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Duyuru Görüntüle</h4>
      </div>
      <div class="modal-body" id="modal">
          <div class="form-group col-md-12">
              <label>Duyuru</label>
              <input type="text" class="form-control" name="content" id="noticeEditContent">
          </div>
          <div class="form-group col-md-12">
              <label>Ekler</label>
              <div class="col-md-12 padding-temizle" id="ekler">
              </div>
          </div>
          <div class="form-group">
            <label for="exampleInputPersonel">Dosya Ekle</label>
            <form action="/admin/notice/upload" class="dropzone" id="my-awesome-dropzone-2">
            {!! csrf_field() !!}
            <input type="hidden" name="notice_key" id="modalNoticeKey" value="">
            </form>
          </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Çıkış</button>
          <button type="submit" class="btn btn-primary">Kaydet</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('edit-js')
<script>
/*$('#modalYeniNotice').on('shown.bs.modal', function (e) {
  var myDropzone = new Dropzone(".dropzone", { url: "/admin/notice/upload"});
});
$('#modalEditNotice').on('shown.bs.modal', function (e) {
  var myDropzone = new Dropzone(".dropzone", { url: "/admin/notice/edit"});
});*/

function saveNewNotice()
{
  var content = document.getElementById('content').value;
  var notice_key = document.getElementById('notice_key').value;
  $.ajax({
    url: '/admin/notice/create',
    type: 'POST',
    beforeSend: function (xhr) {
      var token = $('meta[name="csrf_token"]').attr('content');

      if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
      }
    },
    cache: false,
    data: {content: content, notice_key: notice_key },
    success: function(data){
      /*location.href = "/";*/
      location.reload();
      /*alert("Öğrenci Resim Başarıyla Eklendi");*/
    },
    error: function(jqXHR, textStatus, err){}
  });
}

function noticeDetay(notice_key) {
    $.ajax({
        url: '/admin/muhasebe/notice-detay/show',
        type: 'POST',
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        cache: false,
        data: {notice_key: notice_key },
        success: function(data){
            $( "#ekler" ).empty();
            var value;
            var i,say,str,res,iconfile;
            var say;
            var deger = data.split("/");
            console.log(deger);
            /*for(value in deger){
                console.log(deger[value]);
            }*/

            $("#modalNoticeKey").val(deger[0]);
            $("#noticeEditContent").val(deger[1]);
            for (i = 2; i < deger.length; i++) {
              say = i - 1;
              str = deger[i];
              res = str.split(".");
              if (res[1] == "doc" || res[1] == "DOC" || res[1] == "docx" || res[1] == "DOCX") {
                  iconfile = "doc.png";
              }
              else if (res[1] == "pdf" || res[1] == "PDF") {
                  iconfile = "pdf.png";
              }
              else if (res[1] == "jpg" || res[1] == "JPG" || res[1] == "png" || res[1] == "PNG" || res[1] == "gif" || res[1] == "GIF" || res[1] == "bmp" || res[1] == "BMP" || res[1] == "psd" || res[1] == "PSD") {
                  iconfile = "img.png";
              }
              else if (res[1] == "mp3" || res[1] == "m4a" || res[1] == "wav" || res[1] == "aud" || res[1] == "MP3" || res[1] == "wma" || res[1] == "wow") {
                  iconfile = "mp3.png";
              }
              else if (res[1] == "mpeg" || res[1] == "mp4" || res[1] == "avi" || res[1] == "wmv") {
                  iconfile = "mpeg.png";
              }
              else if (res[1] == "ppt" || res[1] == "pptx" || res[1] == "PPT" || res[1] == "PPTX") {
                  iconfile = "ppt.png";
              }
              else if (res[1] == "txt") {
                  iconfile = "txt.png";
              }
              else if (res[1] == "xls" || res[1] == "xlsx" || res[1] == "XLS" || res[1] == "XLSX" || res[1] == "XLSM" || res[1] == "xlsm") {
                  iconfile = "xls.png";
              }
              else if (res[1] == "rar" || res[1] == "RAR" || res[1] == "zip" || res[1] == "ZIP" || res[1] == "iso" || res[1] == "ISO") {
                  iconfile = "rar.png";
              }
              else
              {
                iconfile = "unknown.png";
              }
              $('#ekler').append('<div class="col-md-3"><a href="/img/upload/'+deger[0]+'/'+deger[i]+'"> <div class="row padding-temizle" style="text-align:center;"><img src="/img/dosya-icon/'+iconfile+'" style="width:60px;"></div><div class="row padding-temizle" style="text-align:center;">Dosya Eki '+say+'</div></a></div>');
            }
            //$('#ekler').append(deger[2]);
            $("#modalEditNotice").modal();

        },
        error: function(jqXHR, textStatus, err){}
    });
}
</script>
@endsection
