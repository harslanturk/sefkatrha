@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Misafir Öğrenci Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Misafir Öğrenci Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="
                          @if(isset($ogrenciResim))
                          {{ $ogrenciresim->resim }}
                          @else
                          /img/user.png
                          @endif
                                  " alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <form action="/admin/guest-students/update-resim" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <div class="form-group">
                                      <label for="exampleInputFile"><b>Profil Resmi Değiştir:</b></label>
                                      <input type="file" id="profilResim" name="resim">
                                      <input type="hidden" value="{{ $ogrenci->id }}" name="ogrenci_id" id="presim_ogrenci_id">
                                  </div>
                                  @if($deleg['u']==1)
                                  <div>
                                      <button type="submit" class="btn btn-primary" >Kaydet</button><!--onclick="saveStudentImage()"-->
                                  </div>
                                  @endif
                                  </form>
                              </li>
                          </ul>

                          <a href="/admin/students/show/{{ $ogrenci->id }}" class="btn btn-primary btn-block"><b>İşlemler</b></a>
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title"></h3>
                      </div><!-- /.box-header -->
                      <div role="form">
                          <div class="box-body">
                              <div class="form-group col-md-4">
                                  <label for="inputOgrenciNo">Öğrenci No</label>
                                  <input type="hidden" id="inputOgrenciId" value="{{ $ogrenci->id }}">
                                  <input type="text" class="form-control" id="inputOgrenciNo" placeholder="Öğrenci Numarası" value="{{$ogrenci->ogrenci_no}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputTcNo">TC No</label>
                                  <input type="text" class="form-control" id="inputTcNo" placeholder="TC No" value="{{$ogrenci->tcno}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputEngelTipi">Engel Tipi</label>
                                  <select class="form-control" id="inputEngelTipi">
                                      <option disabled>Lütfen Seçiniz</option>
                                      @foreach($engelTipleri as $value)
                                          @if($ogrenci->engel_tipi==$value->id)
                                          <option selected value="{{ $value->id }}">{{ $value->engel }}</option>
                                          @else
                                          <option value="{{ $value->id }}">{{ $value->engel }}</option>
                                          @endif
                                      @endforeach
                                  </select>
                                  <!--<input type="text" class="form-control" id="inputEngelTipi" placeholder="Engel Tipi" value="{{$ogrenci->engel_tipi}}">-->
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAdi">Adı</label>
                                  <input type="text" class="form-control" id="inputAdi" placeholder="Adı" value="{{$ogrenci->ad}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputSoyadi">Soyadı</label>
                                  <input type="text" class="form-control" id="inputSoyadi" placeholder="Soyadı" value="{{$ogrenci->soyad}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputCinsiyeti">Cinsiyeti</label>
                                  <input type="text" class="form-control" id="inputCinsiyeti" placeholder="Cinsiyeti" value="{{$ogrenci->cinsiyet}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputDogumTarihi">Doğum Tarihi</label>
                                  <input type="date" class="form-control" id="inputDogumTarihi" placeholder="Doğum Tarihi" value="{{$ogrenci->dogum_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputDogumYeri">Doğum Yeri</label>
                                  <input type="text" class="form-control" id="inputDogumYeri" placeholder="Doğum Yeri" value="{{$ogrenci->dogum_yeri}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputOkulunAdi">Okulun Adı</label>
                                  <input type="text" class="form-control" id="inputOkulunAdi" placeholder="Okulun Adı" value="{{$ogrenci->dogum_okul}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputBabaAdi">Baba Adı Soyadı</label>
                                  <input type="text" class="form-control" id="inputBabaAdi" placeholder="Baba Adı Soyadı" value="{{$ogrenci->baba}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputBabaTel">Baba Tel</label>
                                  <input type="text" class="form-control" id="inputBabaTel" placeholder="Baba Tel" value="{{$ogrenci->baba_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputBabaTel">Baba Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputBabaTel" value="{{$ogrenci->baba_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputKayitTarihi">Kayıt Tarihi</label>
                                  <input type="date" class="form-control" id="inputKayitTarihi" placeholder="Kayıt Tarihi" value="{{$ogrenci->kayit_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAnneAdi">Anne Adı Soyadı</label>
                                  <input type="text" class="form-control" id="inputAnneAdi" placeholder="Anne Adı Soyadı" value="{{$ogrenci->anne}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputAnneTel">Anne Tel</label>
                                  <input type="text" class="form-control" id="inputAnneTel" placeholder="Anne Tel" value="{{$ogrenci->anne_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputAnneTel">Anne Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputAnneTel" value="{{$ogrenci->anne_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputAyrilisTarihi">Ayrılış Tarihi</label>
                                  <input type="date" class="form-control" id="inputAyrilisTarihi" placeholder="Ayrılış Tarihi" value="{{$ogrenci->ayrilis_tarihi}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputEgitimSekli">Eğitim Şekli</label>
                                  <input type="text" class="form-control" id="inputEgitimSekli" placeholder="Eğitim Şekli" value="{{$ogrenci->egitim}}">
                              </div>
                              <!--<div class="form-group col-md-4">
                                  <label for="inputDigerTel">Diğer Tel</label>
                                  <input type="text" class="form-control" id="inputDigerTel" placeholder="Diğer Tel" value="{{$ogrenci->diger_tel}}">
                              </div>-->
                              <div class="form-group col-md-4">
                                  <label  for="inputDigerTel">Diğer Tel</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-phone"></i>
                                      </div>
                                      <input type="text" class="form-control" id="inputDigerTel" value="{{$ogrenci->diger_tel}}" data-inputmask='"mask": "(999) 999 99 99"' data-mask>
                                  </div><!-- /.input group -->
                              </div><!-- /.form group -->
                              <div class="form-group col-md-4">
                                  <label for="inputIl">İl</label>
                                  <input type="text" class="form-control" id="inputIl" placeholder="İl" value="{{$ogrenci->il}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputIlce">İlçe</label>
                                  <input type="text" class="form-control" id="inputIlce" placeholder="İlçe" value="{{$ogrenci->ilce}}">
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="inputAdres">Adres</label>
                                  <input type="text" class="form-control" id="inputAdres" placeholder="Adres" value="{{$ogrenci->adres}}">
                              </div>
                          </div><!-- /.box-body -->
                          <div class="box-footer">
                              @if($deleg['u']==1)<button type="submit" class="btn btn-primary" onclick="saveStudent()">Kaydet</button>@endif
                          </div>
                      </div>
                  </div><!-- /.box -->
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()