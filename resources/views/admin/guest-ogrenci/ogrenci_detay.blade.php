@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Misafir Öğrenci Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Misafir Öğrenci Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="/img/user4-128x128.jpg" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Öğrenci No:</b> <a class="pull-right">{{ $ogrenci->ogrenci_no }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>T.C. Kimlik No:</b> <a class="pull-right">{{ $ogrenci->tcno }}</a>
                              </li>
                          </ul>
                          <a href="/admin/students/edit/{{ $ogrenci->id }}" class="btn btn-primary btn-block none-print"><b>Detaylar</b></a>
                          @if(App\Helpers\helper::authControl('tum-misafir-ogrenciler', 'delete'))
                          <a href="#" onclick="deleteApprove('/admin/students/delete/{{ $ogrenci->id }}')" class="btn btn-danger btn-block none-print"><b>Sil</b></a>
                          @endif
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Son Açılan 10 Konu</h3>
                          <span class="pull-right none-print">
                              <div class="btn-group-vertical">
                                  <button type="button" class="btn btn-success" onClick="window.print()">
                                      <i class="fa fa-print"></i>
                                      Yazdır
                                  </button>
                              </div>
                          </span>
                          <button class="btn btn-primary pull-right none-print" data-toggle="modal" data-target="#modalYeniKonu" style="width: 20%; margin-right:20px;"><b>Yeni Konu</b></button>
                      </div><!-- /.box-header -->
                      <div class="box-body no-padding">
                          <table class="table table-striped">
                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>Tarih</th>
                                  <th>Konu</th>
                                  <th>Personel Adı</th>
                                  <th>İşlem</th>
                                  <th>Durum</th>
                                  <th>Açıklama</th>
                              </tr>
                              <?php $sayac=1; ?>
                              @foreach($islemDetay as $key => $value)
                              <tr onclick="document.location = '/admin/process/show/{{ $value->id }}';" style="cursor: pointer;">
                                  <td>{{ $sayac }}</td>
                                  <td><?php $newdate = App\Helpers\Helper::DateConverter($value->created_at); echo $newdate; ?></td>
                                  <td>{{ $value->konu_islem }}</td>
                                  <td>{{ $value->sorumlu }}</td>
                                  <td>{{ $value->islem }}</td>
                                  @if($value->status==1)
                                  <td style="color:green;">Açık</td>
                                  @else
                                  <td style="color:red;">Kapalı</td>
                                  @endif
                                  <td>{{ $value->aciklama }}</td>
                              </tr>
                              <?php $sayac++; ?>
                              @endforeach

                          </table>
                      </div><!-- /.box -->
                  </div><!-- /.box -->
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="modalYeniKonu" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/process/saveKonu" method="POST">
            {{ csrf_field() }}
        <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni Konu</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputKonu">Konu</label>
                            <select class="form-control select2" style="width:100%;" id="inputKonu" name="konu_id">
                                <option disabled selected>Lütfen Seçiniz</option>
                                <?php $kontrol=0; ?>
                                @foreach($allogrenciIslemleri as $value)
                                    @foreach($islemDetaySp as $spvalue)
                                        @if($spvalue->konu_islem == $value->islem)
                                            <?php $kontrol =1; ?>
                                        @endif
                                    @endforeach
                                    @if($kontrol==0)
                                            <option value="{{ $value->id }}">{{ $value->islem }}</option>
                                    @endif
                                    <?php $kontrol=0; ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Görevlendirilen Personel</label>
                            <select class="form-control select2" style="width:100%;" id="inputKonuPersonel" name="alici_id">
                                <option disabled selected>Lütfen Seçiniz</option>
                                @foreach($allpersonel as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputIslem">İşlem</label>
                            <select class="form-control" name="islem" id="inputKonuIslem">
                                <option>Yeni İşlem</option>
                                <!--<option>İşlem Yapılıyor</option>
                                <option>Onaylandı</option>
                                <option>Red</option>-->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Açıklama</label>
                            <textarea class="form-control" rows="3" placeholder="Açıklama" name="aciklama"></textarea>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop()