@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Güncelleme Notları<small>Test</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li><a href="#">Güncelleme Notları</a></li
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <!-- The time line -->
                    <ul class="timeline">
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-green">
                            15 Kasım 2016
                          </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Personel sayfası düzeltildi.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    İşlem sayfasındaki hatalar giderildi.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Güncelleme notları modülü eklendi.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Giriş sayfasındaki güvenlik açıkları kapatıldı.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Mesajlar sayfasındaki hatalar düzeltildi. Sunucuda test edilecek.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Kod blokları düzeltildi.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    İconlar düzeltildi.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item --><!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                <h3 class="timeline-header"><a href="#">Destek Ekibi</a></h3>
                                <div class="timeline-body">
                                    Uyarı ekranları yüklendi. Test ediliyor.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">Beğen</a>
                                    <a class="btn btn-danger btn-xs">Beğenme</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection