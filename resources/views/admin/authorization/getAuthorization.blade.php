@extends('admin.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  {{$group->name}} Grubu Yetkileri
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/users"><i class="fa fa-dashboard active"></i> Üyeler</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-lg-12">
			<div class="box box-primary" style="padding: 1%;">
			<div class="box-header">
        <form class="form-group" action="{{URL::to('/admin/authorization/createAuth')}}" method="POST">
          {{ csrf_field() }}
          <div class="box-header with-border" style="margin-bottom: 1%">
            <h3 class='box-title'>Yetkilendirilecek Menüler</h3>
            <table class='table no-margin'>
              <tr>
                <td class="col-sm-2">&nbsp;</td>
                <td class="col-sm-3">&nbsp;</td>
                <td class="col-sm-2"> <input type="checkbox" class="Allread"> </td>
                <td class="col-sm-2"> <input type="checkbox" class="Alladd"> </td>
                <td class="col-sm-2"> <input type="checkbox" class="Allupdate"> </td>
                <td class="col-sm-2"> <input type="checkbox" class="Alldelete"> </td>
              </tr>
            </table>
          </div>
          <table class='table table-bordered table-hover'>
            <thead>
              <th>#</th>
              <th>Kolon Adı</th>
              <th>Listeleme</th>
              <th>Ekleme</th>
              <th>Güncelleme</th>
              <th>Silme</th>
            </thead>
            <?php $sira=1;
              $selected ="";
            ?>
            <!-- @if($yetkilers) -->
            @foreach($yetkilers as $yetkiler)
            <?php
            $modul_id = 0;
              if (!$yetkiler->modul_id) {
                $modul_id = $yetkiler->m_id;
              }else{
                  $modul_id = $yetkiler->m_id;
              }
             ?>
            <input type="hidden" name="group_id" value="{{$group_id}}">
              <tr>
                <td>{{$sira}}</td>
                <td>{{$yetkiler->name}}</td>
                @if($yetkiler->read == 1)
                <?php $selected="checked";?>
                @endif
                <td>
                  <label>
                        <input type="hidden" class="read" name="read[{{$modul_id}}]" value="0">
                        <input type="checkbox" class="read" name="read[{{$modul_id}}]" class="minimal" value="1" {{$selected}}>
                  </label>
                </td>
                <?php $selected="";?>
                @if($yetkiler->add == 1)
                <?php $selected="checked";?>
                @endif
                <td>
                  <label>
                        <input type="hidden" class="add" name="add[{{$modul_id}}]" value="0">
                        <input type="checkbox" class="add" name="add[{{$modul_id}}]" class="minimal" value="1" {{$selected}}>
                  </label>
                </td>
                <?php $selected="";?>
                @if($yetkiler->update == 1)
                <?php $selected="checked";?>
                @endif
                <td>
                  <label>
                        <input type="hidden" class="update" name="update[{{$modul_id}}]" value="0">
                        <input type="checkbox" class="update" name="update[{{$modul_id}}]" class="minimal" value="1" {{$selected}}>
                  </label>
                </td>
                <?php $selected="";?>
                @if($yetkiler->delete == 1)
                <?php $selected="checked";?>
                @endif
                <td>
                  <label>
                        <input type="hidden" class="delete" name="delete[{{$modul_id}}]" value="0">
                        <input type="checkbox" class="delete" name="delete[{{$modul_id}}]" class="minimal" value="1" {{$selected}}>
                  </label>
                </td>
              </tr>
              <?php $sira++;
                $selected =""; ?>
            @endforeach
            <!-- @else
            @foreach($modules as $modul)
            <input type="hidden" name="group_id" value="{{$group_id}}">
              <tr>
                <td>{{$sira}}</td>
                <td>{{$modul->name}}</td>
                <td>
                  <label>
                        <input type="hidden" class="read" name="read[{{$modul->id}}]" value="0" id="read">
                        <input type="checkbox" class="read" name="read[{{$modul->id}}]" class="minimal" value="1" id="read">
                  </label>
                </td>
                <td>
                  <label>
                        <input type="hidden" class="add" name="add[{{$modul->id}}]" value="0">
                        <input type="checkbox" class="add" name="add[{{$modul->id}}]" class="minimal" value="1">
                  </label>
                </td>
                <td>
                  <label>
                        <input type="hidden" class="update" name="update[{{$modul->id}}]" value="0">
                        <input type="checkbox" class="update" name="update[{{$modul->id}}]" class="minimal" value="1">
                  </label>
                </td>
                <td>
                  <label>
                        <input type="hidden" class="delete" name="delete[{{$modul->id}}]" value="0">
                        <input type="checkbox" class="delete" name="delete[{{$modul->id}}]" class="minimal" value="1">
                  </label>
                </td>
              </tr>
              <?php //$sira++;?>
              @endforeach
            @endif -->
          </table>
          <div class='form-group'>
            <label>
              <button class='btn btn-primary'><i class="fa fa-save"></i> Kaydet</button>
            </label>
          </div>
        </select>
        </div>
        </form>
			</div>
			</div>
			</div>
		</div>
	</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- EngelTipGuncelle Modal -->
@endsection
@section('edit-js')
  <script type="text/javascript">
    $(':checkbox.Allread').change(function(){
    $(':checkbox.read').prop('checked', this.checked);
    });
    $(':checkbox.Allupdate').change(function(){
    $(':checkbox.update').prop('checked', this.checked);
    });
    $(':checkbox.Alldelete').change(function(){
    $(':checkbox.delete').prop('checked', this.checked);
    });
    $(':checkbox.Alladd').change(function(){
    $(':checkbox.add').prop('checked', this.checked);
    });
  </script>
@endsection
