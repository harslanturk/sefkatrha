@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Planlama Yazdır
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama Yazdır</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
             @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('error')}}</strong>
                </div>
            @elseif(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>{{Session::get('success')}}</strong>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div class="box box-solid">
                        <div class="box-header with-border text-center">
                            <h3 class="box-title text-danger">Hafta Seçiniz</h3>
                            <div class="box-tools">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                          <ul class="nav nav-pills nav-stacked">
                            @foreach($plans as $plan)
                              <li>
                                <a href="/admin/yazdir/sinif/{{$plan->id}}">{{$plan->hafta}}</a>
                              </li>
                            @endforeach
                          </ul>
                        </div><!-- /.box-body -->
                    </div><!-- /. box -->
                </div><!-- /.col -->
                @if($sinif)
                <div class="col-md-3" id="sinif_menu">
                    <div class="box box-solid">
                        <div class="box-header with-border text-center">
                            <h3 class="box-title">Sınıf Seçiniz</h3>
                            <div class="box-tools">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                          <ul class="nav nav-pills nav-stacked">
                            <li>
                              <a href="/admin/yazdir/sinif/print/{{$plan_id}}/Z" target="_blank">Zihinsel Engelli</a>
                            </li>
                            <li>
                              <a href="/admin/yazdir/sinif/print/{{$plan_id}}/F" target="_blank">Fiziksel Engelli</a>
                            </li>
                          </ul>
                        </div><!-- /.box-body -->
                    </div><!-- /. box -->
                </div><!-- /.col -->
                @endif
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection
