@extends('admin.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- ALERT -->
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
    @endif
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                SMS
                <small>SMS Listesi</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li><a href="/admin/sms"><i class="fa fa-dashboard active"></i> Sms</a></li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs text-bold">
                                <li class="active"><a href="#secerek_gonder" data-toggle="tab" aria-expanded="true">Öğrencilere Gönder</a></li>
                                <li class=""><a href="#personel_gonder" data-toggle="tab" aria-expanded="false">Personellere Gönder</a></li>
                                <li class=""><a href="#elle_gonder" data-toggle="tab" aria-expanded="false">Elle Gönder</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="secerek_gonder">
                                    <form class="form-horizontal" method="post" action="{{URL::to('/admin/sms/create')}}">
                                        {!! csrf_field() !!}
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Sms :</label>
                                                <span class="label label-danger"> 5000 Adet</span>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3"></div>
                                                <div class="callout callout-danger col-sm-6 text-center">
                                                    <strong>Bu Alandan Seçtiğiniz Kişilere SMS Gönderebilirsiniz.</strong>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Karakter :</label>
                                                <label class="control-label" id="kalan_karakter">160</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Mesajınız :</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" onkeyup="sms_gonder_elle()" id="sms_mesaj_secerek" name="sms_detail_secerek" rows="5" style="resize: none;">Buraya mesajınızı yazınız.</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" id="btn_secerek_gonder" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Sms Gönder</button>
                                            </div>
                                        </div>
                                    <!-- Post -->
                                    <div class="post">
                                        <table id="hastalar_table" class="table table-bordered table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Sıra</th>
                                                <th>Ad - Soyad</th>
                                                <th>Seç</th>
                                                <th>Baba Telefon</th>
                                                <th>Seç</th>
                                                <th>Anne Telefon</th>
                                                <th>Seç</th>
                                                <th>Diğer Telefon</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no=1;?>
                                            @foreach($students as $student)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$student->ad}} {{$student->soyad}}</td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="baba_id[{{$student->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$student->baba_tel}} <input type="hidden" name="baba_tel[{{ $student->id }}]" value="{{ $student->baba_tel }}"></td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="anne_id[{{$student->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$student->anne_tel}} <input type="hidden" name="anne_tel[{{ $student->id }}]" value="{{ $student->anne_tel }}"></td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="diger_id[{{$student->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$student->diger_tel}} <input type="hidden" name="diger_tel[{{ $student->id }}]" value="{{ $student->diger_tel }}"></td>
                                                </tr>
                                                <?php $no++;?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </form>
                                </div><!-- /.tab-pane -->
                                <div class="tab-pane" id="personel_gonder">
                                    <form class="form-horizontal" method="post" action="{{URL::to('/admin/sms/personel')}}">
                                        {!! csrf_field() !!}
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Sms :</label>
                                                <span class="label label-danger"> 5000 Adet</span>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3"></div>
                                                <div class="callout callout-danger col-sm-6 text-center">
                                                    <strong>Bu Alandan Seçtiğiniz Kişilere SMS Gönderebilirsiniz.</strong>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Karakter :</label>
                                                <label class="control-label" id="kalan_karakter_1">160</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Mesajınız :</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" onkeyup="sms_gonder_elle_3()" id="sms_mesaj_personel" name="sms_detail_secerek" rows="5" style="resize: none;">Buraya mesajınızı yazınız.</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" id="btn_secerek_gonder" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Sms Gönder</button>
                                            </div>
                                        </div>
                                    <!-- Post -->
                                    <div class="post">
                                        <table id="hastalar_table" class="table table-bordered table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Sıra</th>
                                                <th>Ad - Soyad</th>
                                                <th>Seç</th>
                                                <th>Telefon</th>
                                                <th>Seç</th>
                                                <th>İş Telefonu</th>
                                                <th>Seç</th>
                                                <th>Diğer Telefon</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no=1;?>
                                            @foreach($staffs as $staff)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$staff->name}}</td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="tel_id[{{$staff->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$staff->telefon}} <input type="hidden" name="tel[{{ $staff->id }}]" value="{{ $staff->telefon }}"></td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="istel_id[{{$staff->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$staff->is_tel}} <input type="hidden" name="is_tel[{{ $staff->id }}]" value="{{ $staff->is_tel }}"></td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" name="diger_id[{{$staff->id}}]" class="minimal">
                                                        </label>
                                                    </td>
                                                    <td>{{$staff->diger_tel}} <input type="hidden" name="diger_tel[{{ $staff->id }}]" value="{{ $staff->diger_tel }}"></td>
                                                </tr>
                                                <?php $no++;?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </form>
                                </div><!-- /.tab-pane -->
                                <div class="tab-pane" id="elle_gonder">
                                    <form class="form-horizontal" method="post" action="{{URL::to('/admin/sms/elle')}}">
                                        {!! csrf_field() !!}
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Sms :</label>
                                                <span class="label label-danger"> 5000 Adet</span>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3"></div>
                                                <div class="callout callout-danger col-sm-6 text-center">
                                                    <strong>Bu Alandan Numarasını Yazdığınız Kişiye Sms Gönderebilirsiniz !</strong>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Kalan Karakter :</label>
                                                <label class="control-label" id="kalan_karakter_2">160</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Telefon Numarası :</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="elle_phone" id="elle_phone" data-inputmask='"mask": "09999999999"' data-mask>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2">Mesajınız :</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" onkeyup="sms_gonder_elle_2()" id="sms_mesaj_elle" name="sms_detail_elle" rows="5" style="resize: none;">Buraya mesajınızı yazınız.</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" id="btn_elle_gonder" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Sms Gönder</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.tab-content -->
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
@endsection