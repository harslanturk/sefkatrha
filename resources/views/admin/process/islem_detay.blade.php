@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
      "{{ $islemDetay->islem }}" Konulu İşlem Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">İşlem Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="@if(isset($ogrenciResim))
                          {{ $ogrenciResim->resim }}
                          @else
                          /img/user.png
                          @endif" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Öğrenci No:</b> <a class="pull-right">{{ $ogrenci->ogrenci_no }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>T.C. Kimlik No:</b> <a class="pull-right">{{ $ogrenci->tcno }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>Konu:</b> <a class="pull-right">{{ $islemDetay->islem }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>Oluşturan Personel:</b> <a class="pull-right">{{ $personel->name }}</a>
                              </li>
                          </ul>
                          @if($ogrenci->status == 2)
                          <a href="/admin/guest-students/show/{{ $ogrenci->id }}" class="btn btn-primary btn-block none-print"><b>Tüm Konular</b></a>
                          @else
                          <a href="/admin/students/show/{{ $ogrenci->id }}" class="btn btn-primary btn-block none-print"><b>Tüm Konular</b></a>
                          @endif
                          @if($islemDetay->status == 1)
                              @if($sorumlu == Auth::user()->id)
                              <button class="btn btn-success pull-right none-print" data-toggle="modal" data-target="#modalYeniKonu" style="width: 100%; margin-top:10px;"><b>Yeni İşlem</b></button>
                              @endif
                          @else
                          <a href="#" onclick="konuOpenApprove('/admin/process/open/{{ $islemDetay->id }}')" class="btn btn-danger btn-block none-print"><b>Konuyu Aç</b></a>
                          @endif
                          <button class="btn btn-info pull-right none-print" data-toggle="modal" data-target="#modalYeniSMS" style="width: 100%; margin-top:10px;"><b>SMS Gönder</b></button>
                          @if(isset($remind))
                              <button class="btn btn-warning pull-right none-print" data-toggle="modal" data-target="#modalGosterRemind" style="width: 100%; margin-top:10px;"><b>Hatırlatma Göster</b></button>
                          @else
                              <button class="btn btn-warning pull-right none-print" data-toggle="modal" data-target="#modalYeniRemind" style="width: 100%; margin-top:10px;"><b>Hatırlatma Ekle</b></button>
                          @endif
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Tüm İşlemler</h3>
                          <span class="pull-right none-print">
                              <div class="btn-group-vertical">
                                  <button type="button" class="btn btn-success" onClick="window.print()">
                                      <i class="fa fa-print"></i>
                                      Yazdır
                                  </button>
                              </div>
                          </span>
                      </div><!-- /.box-header -->
                      <div class="box-body table-responsive no-padding">
                          <table class="table table-striped">
                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>Tarih</th>
                                  <th>Sorumlu</th>
                                  <th>İşlem</th>
                                  <th>Açıklama</th>
                              </tr>
                              <tr style="background-color:#00c0ef; color:#FFF;">
                                  <td>1</td>
                                  <td><?php $newdate = App\Helpers\Helper::DateConverter($islemDetay->created_at); echo $newdate; ?></td>
                                  <td>{{ $islemDetay->sorumlu }}</td>
                                  <td>Konu Açıldı</td>
                                  <td></td>
                              </tr>
                              <!--<tr style="background-color:#3c8dbc; color:#FFF;">
                                  <td>1</td>
                                  <td><?php $newdate = App\Helpers\Helper::DateConverter($islemDetay->created_at); echo $newdate; ?></td>
                                  <td>{{ $islemDetay->alici }}</td>
                                  <td>Yeni İşlem</td>
                                  <td>{{ $islemDetay->aciklama }}</td>
                              </tr>-->
                              <?php $sayac=2; ?>
                              @foreach($islemDetayList as $key => $value)
                                  <tr
                                  @if($value->islem == "İşlem Yapılıyor")
                                      style="background-color:#3c8dbc !important; color:#FFF;"
                                  @elseif($value->islem == "Onaylandı")
                                      style="background-color:#00a65a !important; color:#FFF;"
                                  @elseif($value->islem == "Red")
                                      style="background-color:#dd4b39 !important; color:#FFF;"
                                  @endif
                                  >
                                      <td>{{ $sayac }}</td>
                                      <td><?php $newdate = App\Helpers\Helper::DateConverter($value->created_at); echo $newdate; ?></td>
                                      <td>{{ $value->alici }}</td>
                                      <td>{{ $value->islem }}</td>
                                      <td>{{ $value->aciklama }}</td>
                                  </tr>
                                  <?php $sayac++; ?>
                              @endforeach
                          </table>
                      </div><!-- /.box -->
                  </div><!-- /.box -->
                  @if(isset($remind))
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Hatırlatma</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body no-padding">
                          <table class="table table-striped">
                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>Tarih</th>
                                  <th>Hatırlatma Notu</th>
                                  <th>İşlem</th>
                              </tr>
                              <tr style="background-color:#f39c12; color:#FFF;">
                                  <td>1</td>
                                  <td><?php $newdate = App\Helpers\Helper::DateConverter($remind->remind_date); echo $newdate; ?></td>
                                  <td>{{ $remind->content }}</td>
                                  @if(Auth::user()->id == $remind->user_id)
                                  <td><a class="button btn btn-danger none-print" onclick="deleteApprove('/admin/remind/delete/{{ $remind->id }}')"><i class="fa fa-trash"> Sil</i></a></td>
                                  @else
                                  <td></td>
                                  @endif
                              </tr>
                          </table>
                      </div><!-- /.box -->
                  </div><!-- /.box -->
                  @endif
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="modalYeniKonu" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/process/saveIslem" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="islem_detay" value="{{ $islemDetay->id }}">
            <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
            <input type="hidden" name="konu" value="{{ $islemDetay->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni İşlem</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputKonu">Konu</label>
                            <p style="font-size:14px;">{{ $islemDetay->islem }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Görevlendirilen Personel</label>
                            <select class="form-control select2" style="width:100%;" id="personelCek" name="alici_id">
                                <option disabled selected>Lütfen Seçiniz</option>
                                @foreach($allpersonel as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputIslem">İşlem</label>
                            <select class="form-control" name="islem" id="islemCek">
                                <option disabled>Önce Personel Seçiniz</option>
                                <!--<option>İşlem Yapılıyor</option>
                                <option>Onaylandı</option>
                                <option>Red</option>-->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Açıklama</label>
                            <textarea class="form-control" rows="3" placeholder="Açıklama" name="aciklama" maxlength="160" onkeyup="islem_gonder_elle()" id="islem_mesaj_secerek"></textarea>
                        </div>
                        <div class="form-group" style="text-align:right">
                            <label class="control-label">Kalan Karakter :<span class="control-label" id="islem_kalan_karakter">160</span></label>

                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- SMS Modal -->
<div class="modal fade" id="modalYeniSMS" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/sms/saveIslemSMS" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="islem_detay" value="{{ $islemDetay->id }}">
            <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
            <input type="hidden" name="konu" value="{{ $islemDetay->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">SMS Gönder</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputKonu">Konu</label>
                            <p style="font-size:14px;">{{ $islemDetay->islem }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputKonu">Personel</label>
                            <select class="form-control select2" multiple="multiple" data-placeholder="Personel Seç" style="width: 100%;">
                                @foreach($allPersonel as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputKonu">Veli</label>
                            <select class="form-control select2" multiple="multiple" data-placeholder="Veli Seç" style="width: 100%;">
                                <option value="{{$ogrenci->anne_tel}}">Anne: {{$ogrenci->anne_tel}}</option>
                                <option value="{{$ogrenci->baba_tel}}">Baba: {{$ogrenci->baba_tel}}</option>
                                <option value="{{$ogrenci->diger_tel}}">Diğer: {{$ogrenci->diger_tel}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputIslem">İşlem</label>
                            <select class="form-control" name="islem" id="inputKonuIslem">
                                @foreach($smsIslemDetayList as $smsvalue)
                                <option>{{ $smsvalue->islem }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Açıklama</label>
                            <textarea class="form-control" rows="3" placeholder="Açıklama" name="aciklama" onkeyup="sms_gonder_elle()" id="sms_mesaj_secerek"></textarea>
                        </div>
                        <div class="form-group" style="text-align:right">
                            <label class="control-label">Kalan Karakter :<span class="control-label" id="kalan_karakter">160</span></label>

                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Hatırlatma Ekle Modal -->
<div class="modal fade" id="modalYeniRemind" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/remind/save" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
            <input type="hidden" name="konu" value="{{ $islemDetay->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hatırlatma Ekle</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputKonu">Öğrenci</label>
                            <p style="font-size:14px;">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputKonu">Konu</label>
                            <p style="font-size:14px;">{{ $islemDetay->islem }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Hatırlatma Tarihi</label>
                            <input type="date" class="form-control" name="remind_date">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Hatırlatma Notu</label>
                            <textarea class="form-control" rows="3" placeholder="Not" name="content"></textarea>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@if(isset($remind))
<!-- Hatırlatma Göster Modal -->
<div class="modal fade" id="modalGosterRemind" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hatırlatma</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputKonu">Öğrenci</label>
                        <p style="font-size:14px;">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputKonu">Konu</label>
                        <p style="font-size:14px;">{{ $islemDetay->islem }}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPersonel">Hatırlatma Tarihi</label>
                        <p style="font-size:14px;">{{ $remind->remind_date }}</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPersonel">Hatırlatma Notu</label>
                        <p style="font-size:14px;">{{ $remind->content }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                @if(Auth::user()->id == $remind->user_id)
                <button type="submit" class="btn btn-danger" onclick="deleteApprove('/admin/remind/delete/{{ $remind->id }}')">Sil</button>
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
<script src="/js/newprocess.js"></script>
@stop()
