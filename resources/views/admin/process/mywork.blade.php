@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- ALERT -->
@if (Session::has('flash_notification.message'))
  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('flash_notification.message') }}
  </div>
@endif
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    İşlerim <small>Size atanan bütün konular bu sayfada listelenmektedir.</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">İşlerim</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="box box-primary" style="padding: 1%;">
        <div class="box-header">
          <h3 class="box-title">Filtre</h3>
        </div>
        <form action="/admin/process/search" method="post">
        {{ csrf_field() }}
        <table class="table table-hover table-bordered">
        <thead>
        <tr>
          @if($delegation_id->delegation_id == 1)
          <th>Kullanıcılar</th>
          @endif
          <th>Konu</th>
          <th>İşlem</th>
          <th>İşlemler</th>
        </tr>
        </thead>
        <tbody>
          <tr>
            @if($delegation_id->delegation_id == 1)
            <td>
              <select required class="form-control select2" style="width:100%;" id="kullanici_id" name="kullanici">
                  <option disabled selected>Lütfen Seçiniz</option>
                  <option>Tüm Kullanıcılar</option>
                  @foreach($kullanicilar as $kullanici)
                      <option value="{{ $kullanici->id }}">{{ $kullanici->name }}</option>
                  @endforeach
              </select>
            </td>
            @endif
            <td>
              <select required class="form-control select2" style="width:100%;" id="konu_id" name="konu_id">
                  <option disabled selected>Lütfen Seçiniz</option>
                  <option>Tüm Konular</option>
                  @foreach($allogrenciIslemleri as $ogrenciIslem)
                      <option value="{{ $ogrenciIslem->id }}">{{ $ogrenciIslem->islem }}</option>
                  @endforeach
              </select>
            </td>
            <td>
              <select required name="islem" class="form-control">
                <option disabled selected>Lütfen Seçiniz</option>
                <option>Tüm İşlemler</option>
                <option>İşlem Yapılıyor</option>
                <option>Onaylandı</option>
                <option>Red</option>
              </select>
            </td>
            <td>
             <button type="submit" class="btn btn-primary">Ara</button>
            </td>
          </tr>
        </tbody>
        </table>
        </form>
      </div>
    </div>
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
      <div class="box-header">
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="ogrenci-table" class="table table-bordered table-hover table-striped display responsive nowrap">
          <thead>
            <tr>
              <th>#</th>
              <th>Tarih</th>
              <th>Öğrenci</th>
              <th>Konu</th>
              <th>Görevlendiren</th>
              <th>İşlem</th>
            </tr>
          </thead>
          <tbody>
          @foreach($islemDetay as $key => $value)
            <tr onclick="document.location = '/admin/process/show/{{ $value->konu_id }}';" style="cursor: pointer;">
              <td><?= $key+1; ?>
              <td><?php $newdate = App\Helpers\Helper::DateConverter($value->created_at); echo $newdate; ?></td>
              <td>{{ $value->ogrenci_ad }} {{ $value->ogrenci_soyad }} </td>
              <td>{{ $value->konu_islem }}</td>
              <td>{{ $value->sorumlu }}</td>
              <td>{{ $value->islem }}</td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Tarih</th>
              <th>Öğrenci</th>
              <th>Konu</th>
              <th>Görevlendiren</th>
              <th>İslem</th>
            </tr>
          </tfoot>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop()
