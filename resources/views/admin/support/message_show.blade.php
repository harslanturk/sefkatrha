@extends('admin.master')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gelen Kutusu
        <small>13 yeni mesaj</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li class="active">Gelen Kutusu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="/mesajlar/mesaj-olustur" class="btn btn-primary btn-block margin-bottom">Mesaj Oluştur</a>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Dosyalar</h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="/mesajlar"><i class="fa fa-inbox"></i> Gelen Kutusu <span class="label label-primary pull-right">12</span></a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> Giden Kutusu</a></li>
                <li><a href="#"><i class="fa fa-trash-o"></i> Çöp</a></li>
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Etiket</h3>
              <div class="box-tools">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Önemli</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Etkinlik</a></li>
                <!--<li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>-->
              </ul>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
        <form action="/mesajGonder" method="post">
          <div class="col-md-9">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Mail İçeriği</h3>
                <div class="box-tools pull-right">
                  <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                  <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="mailbox-read-info">
                  <h3>{{ $message->konu }}</h3>
                  <h5>From: {{ $user->name }} <span class="mailbox-read-time pull-right">15 Feb. 2015 11:03 PM</span></h5>
                </div><!-- /.mailbox-read-info -->
                <div class="mailbox-read-message">
                  <?php echo "$message->mesaj"; ?>
                </div><!-- /.mailbox-read-message -->
              </div><!-- /.box-body -->
              <div class="box-footer">
                <button class="btn btn-default"><i class="fa fa-trash-o"></i> Kapat</button>
                <button class="btn btn-default"><i class="fa fa-print"></i> Yazdır</button>
              </div><!-- /.box-footer -->
            </div><!-- /. box -->
          </div><!-- /.col -->
        </form>
        <div class="col-md-3"></div>
        <div class="col-md-9">
            <div class="col-md-12 padding-temizle" style="background-color:#CCC;border: 5px solid #ccc;">
                @foreach($messagelist as $mesaj)
                    @if($mesaj->gonderen_id == $authuser->id)
                        <div class="col-md-12 padding-temizle">
                          <div class="box box-primary" style="border-top: 10px solid #008d4c;">
                            <div class="box-header with-border">
                              <h3 class="box-title">Giden Mail</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body no-padding">
                              <div class="mailbox-read-info">
                                <h5>From: {{ $authuser->name }} <span class="mailbox-read-time pull-right">{{ $mesaj->created_at }}</span></h5>
                              </div><!-- /.mailbox-read-info -->
                              <div class="mailbox-read-message">
                                <?php echo "$mesaj->mesaj"; ?>
                              </div><!-- /.mailbox-read-message -->
                            </div><!-- /.box-body -->
                          </div><!-- /. box -->
                        </div>
                    @else
                        <div class="col-md-12 padding-temizle">
                            <div class="box box-primary" style="border-top: 10px solid #3c8dbc;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Gelen Mail</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="mailbox-read-info">
                                        <h5>From: {{ $user->name }} <span class="mailbox-read-time pull-right">{{ $mesaj->created_at }}</span></h5>
                                    </div><!-- /.mailbox-read-info -->
                                    <div class="mailbox-read-message">
                                        <?php echo "$mesaj->mesaj"; ?>
                                    </div><!-- /.mailbox-read-message -->
                                </div><!-- /.box-body -->
                            </div><!-- /. box -->
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="col-md-12 padding-temizle" style="margin-top:20px;">
                <form action="/replyMessage" method="post">
                <div class="box box-primary" style="border-top: 10px solid #f39c12;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mesaj Gönder</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="gonderen_id" value="{{ $message->alici_id }}">
                            <input type="hidden" class="form-control" name="alici_id" value="{{ $user->id }}">
                            <input type="hidden" class="form-control" name="message_id" value="{{ $message->id }}">
                        {{ csrf_field() }}
                        <!--<input class="form-control" placeholder="To:" name="alici_id">-->
                        </div>
                        <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 300px" name="mesaj">
                    </textarea>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Gönder</button>
                        </div>
                    </div><!-- /.box-footer -->
                </div><!-- /. box -->
                </form>
            </div><!-- /.col -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
@stop()