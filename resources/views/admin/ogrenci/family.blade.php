@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Öğrenci Detay
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Öğrenci Detay</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-3">
                  <!-- Profile Image -->
                  <div class="box box-primary">
                      <div class="box-body box-profile">
                          <img class="profile-user-img img-responsive img-circle" src="@if(isset($ogrenciResim))
                          {{ $ogrenciResim->resim }}
                          @else
                          /img/user.png
                          @endif" alt="User profile picture">
                          <h3 class="profile-username text-center">{{ $ogrenci->ad }} {{ $ogrenci->soyad }}</h3>
                          <!--<p class="text-muted text-center">Software Engineer</p>-->

                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Öğrenci No:</b> <a class="pull-right">{{ $ogrenci->ogrenci_no }}</a>
                              </li>
                              <li class="list-group-item">
                                  <b>T.C. Kimlik No:</b> <a class="pull-right">{{ $ogrenci->tcno }}</a>
                              </li>
                          </ul>
                          @if(App\Helpers\helper::authControl('ogrenci-detay','read'))
                          <a href="/admin/students/edit/{{ $ogrenci->id }}" class="btn btn-primary btn-block none-print"><b>Detaylar</b></a>
                          @endif
                      </div><!-- /.box-body -->
                  </div><!-- /.box -->
              </div>   <!-- /.row -->
              <div class="col-md-9">
                  <!-- general form elements -->
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Aile Görüşmeleri</h3>
                          <span class="pull-right none-print">
                              <div class="btn-group-vertical">
                              @if(App\Helpers\helper::authControl('ogrenci-yazdir','read'))
                                  <button type="button" class="btn btn-success" onClick="window.print()">
                                      <i class="fa fa-print"></i>
                                      Yazdır
                                  </button>
                                  @endif
                              </div>
                          </span>
                          <button class="btn btn-primary pull-right none-print" data-toggle="modal" data-target="#modalYeniKonu" style="width: 20%; margin-right:20px;"><b>Yeni Görüşme</b></button>
                      </div><!-- /.box-header -->
                      <div class="box-body table-responsive no-padding">
                          <table class="table table-striped">
                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>Tarih</th>
                                  <th>Görevlendirilen</th>
                                  <th>Açıklama</th>
                                  <th>Durum</th>
                              </tr>
                              <tbody>
                                  @foreach($familys as $family)
                                  @if($family->alici_id == Auth::user()->id || Auth::user()->delegation_id == 1)
                                  <tr onclick="document.location = '/admin/students/family-show-detay/{{$family->id}}/{{$ogrenci->id}}';" style="cursor: pointer;">
                                  @else
                                  <tr>
                                  @endif
                                  <td></td>
                                  <td>{{App\Helpers\Helper::DateConverter($family->tarih)}}</td>
                                  <td>{{$family->alici_ad}}</td>
                                  <td>{{$family->aciklama}}</td>
                                  <?php
                                    $durum = App\Helpers\Helper::durumCek($family->id);
                                      //
                                  ?>
                                  @if($durum)
                                  <?php
                                    $islem = App\Helpers\Helper::durumTanimlamasi($durum->durum);
                                  ?>
                                  <td>{{$islem->islem}}</td>
                                  @else
                                  <td>Durum Girilmemiş</td>
                                  @endif
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /.box -->
                  </div><!-- /.box -->
              </div><!--/.col (left) -->
          </div>
      </div>
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="modalYeniKonu" role="dialog">
    <div class="modal-dialog" role="document">
        <form role="form" action="/admin/students/saveFamily" method="POST">
            {{ csrf_field() }}
        <input type="hidden" name="ogrenci_id" value="{{ $ogrenci->id }}">
        <input type="hidden" name="gonderen_id" value="{{ Auth::user()->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Yeni Görüşme</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                    <div class="form-group">
                      <label>Tarih</label>
                      <input class="form-control" type="date" name="tarih">
                    </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Görevlendirilen Kişi</label>
                            <select class="form-control select2" style="width:100%;" id="inputKonuPersonel" name="alici_id">
                                @foreach($allpersonel as $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPersonel">Açıklama</label>
                            <textarea class="form-control" rows="3" placeholder="Açıklama" name="aciklama" maxlength="160" onkeyup="islem_gonder_elle()" id="islem_mesaj_secerek"></textarea>
                        </div>
                        <div class="form-group">
                          <label>
                            <input type="checkbox" class="minimal" value="1" name="gizli"> Gizli
                          </label>
                        </div>
                        <div class="form-group" style="text-align:right">
                            <label class="control-label">Kalan Karakter :<span class="control-label" id="islem_kalan_karakter">160</span></label>

                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop()
