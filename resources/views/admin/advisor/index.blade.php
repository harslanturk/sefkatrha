@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- ALERT -->
@if (Session::has('flash_notification.message'))
  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('flash_notification.message') }}
  </div>
@endif
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Tüm Danışman Notları
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li class="active">Danışman Notları Listesi</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box">
      <div class="box-header">
      @if(App\Helpers\helper::authControl('danisman','add'))
        <h3 class="box-title"> <button class="btn btn-primary pull-right none-print" data-toggle="modal" data-target="#modalYeniDanismanNot" style=""><b>Danışman Notu Ekle</b></button></h3>
        @endif
      </div><!-- /.box-header -->
      <div class="box-body">
        <table id="ogrenci-table" class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th>Sıra</th>
              <th>Tarih</th>
              <th>Danışman Notu</th>
              <th>İşlem</th>
            </tr>
          </thead>
          <tbody>
          <?php $sira=1; ?>
          @foreach($advisors as $advisor)
          <?php
          if($advisor->isread == "0")
          {
            $isread = "primary";
            $eye = "fa-eye-slash";
          }
          else
          {
            $isread = "success";
            $eye = "fa-eye";
          }
          ?>
            <tr>
              <td>{{ $sira }}</td>
              <td><?php $newdate = App\Helpers\Helper::DateTimeConverter($advisor->created_at); echo $newdate; ?></td>
              <td>{{ $advisor->aciklama }}</td>
              <td>
                <a class="button btn btn-{{$isread}}" onclick="readApprove('/admin/advisor/read/{{ $advisor->id }}')" style="margin-right:10px;"><i class="fa {{$eye}}"></i></a>
                @if(App\Helpers\helper::authControl('danisman','delete'))
                <a class="button btn btn-danger" onclick="deleteApprove('/admin/advisor/delete/{{ $advisor->id }}')"><i class="fa fa-trash"></i></a>
                @endif
              </td>
            </tr>
            <?php $sira++; ?>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Sıra</th>
              <th>Tarih</th>
              <th>Danışman Notu</th>
              <th>İşlem</th>
            </tr>
          </tfoot>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal fade" id="modalYeniDanismanNot" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form role="form" action="/admin/advisor/save" method="POST">
      {{ csrf_field() }}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Yeni Danışman Notu</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputPersonel">Danışman Notu</label>
              <textarea class="form-control" rows="3" placeholder="Danışman Notu" name="aciklama"></textarea>
            </div>
          </div><!-- /.box-body -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          <button type="submit" class="btn btn-primary">Kaydet</button>
        </div>
      </div><!-- /.modal-content -->
    </form>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop()
