<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/program/ogrenci-modal-save')}}" method="POST">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Öğrenci Güncelle</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                  <div class="form-group">
                    <input type="hidden" name="id" value="{{$plan->id}}">
                    <label for="">Bilgiler</label>
                    <input class="form-control" type="text" value="{{'Sınıf: '.$plan->sinif_id.' Grup: '.$plan->a_b.' Gün: '.$plan->calisma_gun}}" disabled>
                  </div>
                    <div class="form-group">
                      <label for="exampleInputPersonel">Öğrenci Seç</label>
                      <select class="select2" name="ogrenci_id" style="width:100%;">
                        @foreach($ogrencis as $ogrenci)
                        @if($plan->ogrenci_id == $ogrenci->id)
                        <option value="{{$ogrenci->id}}" selected>{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                        @else
                        <option value="{{$ogrenci->id}}">{{$ogrenci->ad.' '.$ogrenci->soyad}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPersonel">Servis Seç</label>
                        <select class="select2" name="servis_id" style="width:100%;">
                          @foreach($servis as $servi)
                          @if($plan->servis_id == $servi->id)
                          <option value="{{$servi->id}}" selected>{{$servi->name}}</option>
                          @else
                          <option value="{{$servi->id}}">{{$servi->name}}</option>
                          @endif
                          @endforeach
                        </select>
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-success">Güncelle</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
