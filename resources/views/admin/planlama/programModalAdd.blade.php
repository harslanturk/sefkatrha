<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/planlama/saveProgramModal')}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Yeni Planlama</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Hafta Başlangıcı</label>
                        <input class="form-control" type="date" name="start">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Hafta Bitiş</label>
                        <input class="form-control" type="date" name="end">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button id="randevuSaveBtn" type="submit" class="btn btn-primary">Program Ekle</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
