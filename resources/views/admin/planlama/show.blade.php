@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tüm Planlamalar
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary">
                      <form action="/admin/program/createProgram" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="grup_no" value="{{$id}}">
                        <table class="table table-responsive table-bordered">
                        <tr class="text-bold text-center">
                          <td colspan="3">Pazartesi</td>
                          <td colspan="3">Salı</td>
                          <td colspan="3">Çarşamba</td>
                          <td colspan="3">Perşembe</td>
                          <td colspan="3">Cuma</td>
                        </tr>
                        <?php $grup_no = 1;
                        $student = '<option value="0" selected>Öğrenci Seçiniz.</option>';
                        foreach($ogrencis as $key => $ogrenci){
                          $student .='<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
                        }
                        $otobus = '<option value="0" selected>Servis Seçiniz.</option>';
                        foreach($servis as $key => $servi){
                          $otobus .='<option value="'.$servi->id.'">'.$servi->name.'</option>';
                        }
                         ?>
                         <input type="hidden" name="grup_count" value="{{count($grups)}}">
                         <input type="hidden" name="sinif_count" value="{{count($sinifs)}}">
                        @foreach($grups as $key => $grup)
                        <tr class="text-bold text-center" style="background-color:lightgreen;">
                          <td colspan="3">{{$grup->name}}</td>
                          <td colspan="6">
                            {{$grup_no}}.Seans
                            {{$grup->seans_one_start}} -  {{$grup->seans_one_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                          <td colspan="6">
                          <?php $grup_no++; ?>
                            {{$grup_no}}.Seans
                            {{$grup->seans_two_start}} - {{$grup->seans_two_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                        </tr>
                        <?php $grup_no++; ?>
                        <tr>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <td>Öğrenci</td>
                              <td>Servis</td>
                            </table>
                          </td>
                        </tr>
                        @foreach($sinifs as $anahtar => $sinif)
                        <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/pazartesi">
                        <tr>
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                  <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/sali">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/carsamba">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/persembe">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/cuma">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td>a</td>
                              </tr>
                              <tr>
                                <td>b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisa{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    <?php echo $student; ?>
                                  </select>
                                </td>
                                <td>
                                <select class="select2" name="servisb{{$key}}[]" style="width:90%;">
                                <?php echo $otobus; ?>
                                </select>
                              </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        @endforeach
                        @endforeach
                      </table>
                      <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="button">Kaydet</button>
                      </div>
                      </form>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection
@section('edit-js')
<script>
setTimeout(function(){
  $('.sidebar-toggle').click(),1000
});
    $('#AddProgram').click(function () {
        var deger = 'a';
        /*console.log($(this).attr('name'));*/
        $.ajax({
            url: '/admin/planlama/createProgramModal',
            type: 'POST',
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            cache: false,
            data: {deger: deger},
            success: function(data){
                document.getElementById('modalAddProgram').innerHTML=data;
                //RenkSec(10,'color');
                $('.select2').select2();
            },
            error: function(jqXHR, textStatus, err){}
        });
    });
</script>
@endsection
