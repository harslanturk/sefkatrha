<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/program/filter')}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Planlama Getir</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Engel Kategorisi</label>
                <select class="select2" name="engel_kategorisi" style="width:100%;">
                  <option selected disabled>Kategori Seçin</option>
                  <option value="Z">Zihinsel Engelli</option>
                  <option value="F">Bedensel Engelli</option>
                </select>
              </div>
                <div class="form-group">
                  <label>Servis Seçiniz</label>
                  <select class="select2" name="servis_id" style="width:100%;">
                    <option selected disabled>Servis Seçin</option>
                    @foreach($servis as $servi)
                    <option value="{{$servi->id}}">{{$servi->name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-primary">Planlama Getir</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
