<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/planlama/calendarModalSave')}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Yeni Planlama</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="start_date">Öğretmen Seç</label>
                        <select class="select2" name="personel_id" style="width:100%;" required>
                        <option selected disabled>Personel Seç</option>
                          @foreach($personel as $val)
                          <option value="{{$val->id}}">{{$val->name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Öğrenci Seç</label>
                        <select class="select2" name="ogrenci_id" style="width:100%;" required>
                        <option selected disabled>Öğrenci Seç</option>
                          @foreach($ogrenci as $val)
                          <option value="{{$val->id}}">{{$val->ad}} {{$val->soyad}}</option>
                          @endforeach
                        </select>
                    </div>
                      <div class="col-md-6" style="padding:0px;">
                          <label for="start_date">Sınıf Seç</label>
                          <select class="select2" name="sinif" style="width:98%;" required>
                          <option selected disabled>Sınıf Seç</option>
                            <option value="Z1">Z-1</option>
                            <option value="Z2">Z-2</option>
                            <option value="Z3">Z-3</option>
                            <option value="Z4">Z-4</option>
                            <option value="Z5">Z-5</option>
                            <option value="Z6">Z-6</option>
                            <option value="Z7">Z-7</option>
                            <option value="Z8">Z-8</option>
                            <option value="Z9">Z-9</option>
                            <option value="F1">F-1</option>
                            <option value="F2">F-2</option>
                            <option value="F3">F-3</option>
                            <option value="F4">F-4</option>
                          </select>
                      </div>
                      <div class="col-sm-6" style="padding:0px;">
                          <label for="start_date">Servis Seç</label>
                          <select class="select2" name="servis" style="width:98%;" required>
                          <option selected disabled>Servis Seç</option>
                            <option value="servis1">Servis - 1</option>
                            <option value="servis2">Servis - 2</option>
                            <option value="servis3">Servis - 3</option>
                            <option value="servis4">Servis - 4</option>
                          </select>
                      </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="start_date">Başlangıç Tarihi</label>
                        <input class="form-control" type="date" name="start_date">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="start_date">Başlangıç Saati</label>
                        <input class="form-control" type="time" name="start_time">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Bitiş Tarihi</label>
                        <input class="form-control" type="date" name="end_date">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Bitiş Saati</label>
                        <input class="form-control" type="time" name="end_time">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button id="randevuSaveBtn" type="submit" class="btn btn-primary">Planlama Ekle</button>
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
