@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tüm Planlamalar
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary">
                      <div class="pull-right">
                            <button type="button" class="btn btn-success" onClick="window.print()" style="margin:5px;">
                                <i class="fa fa-print"></i>
                                Yazdır
                            </button>
                      </div>
                      <form action="/admin/program/createProgram" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="grup_no" value="{{$id}}">
                        <table class="table table-responsive table-bordered">
                        <tr class="text-bold text-center">
                          <td colspan="3">Pazartesi</td>
                          <td colspan="3">Salı</td>
                          <td colspan="3">Çarşamba</td>
                          <td colspan="3">Perşembe</td>
                          <td colspan="3">Cuma</td>
                        </tr>
                        <?php $grup_no = 1;
                        //$deger = "";
                        // foreach($ogrencis as $key => $ogrenci){
                        //   $deger .='<option value="'.$ogrenci->id.'">'.$ogrenci->ad.' '.$ogrenci->soyad.'</option>';
                        // }
                         ?>
                         <input type="hidden" name="grup_count" value="{{count($grups)}}">
                         <input type="hidden" name="sinif_count" value="{{count($sinifs)}}">
                         <input type="hidden" name="engel_kategorisi" value="{{$engel_kategorisi}}">
                        @foreach($grups as $key => $grup)
                        <tr class="text-bold text-center" style="background-color:lightgreen;">
                          <td colspan="3">{{$grup->name}}</td>
                          <td colspan="6">
                            {{$grup_no}}.Seans
                            {{$grup->seans_one_start}} -  {{$grup->seans_one_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                          <td colspan="6">
                          <?php $grup_no++; ?>
                            {{$grup_no}}.Seans
                            {{$grup->seans_two_start}} - {{$grup->seans_two_end}}  /  {{$grup->mola}} dk Mola
                          </td>
                        </tr>
                        <?php $grup_no++; ?>
                        <tr>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;">
                              <td style="text-align:center;">Öğrenci</td>
                              <td style="text-align:right;padding-right:5px;">Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;">
                              <td style="text-align:center;">Öğrenci</td>
                              <td style="text-align:right;padding-right:5px;">Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;">
                              <td style="text-align:center;">Öğrenci</td>
                              <td style="text-align:right;padding-right:5px;">Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;">
                              <td style="text-align:center;">Öğrenci</td>
                              <td style="text-align:right;padding-right:5px;">Servis</td>
                            </table>
                          </td>
                          <td>Sınıf</td>
                          <td>#</td>
                          <td>
                            <table style="width:100%;">
                              <td style="text-align:center;">Öğrenci</td>
                              <td style="text-align:right;padding-right:5px;">Servis</td>
                            </table>
                          </td>
                        </tr>
                        @foreach($sinifs as $anahtar => $sinif)
                        <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/pazartesi">
                        <tr>
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td class="a_bes">a</td>
                              </tr>
                              <tr>
                                <td class="b_onsekiz">b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <tr>
                                <td style="padding-top:5px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'pazartesi','a',$id,($key+1),$servis_id);

                                    // echo '<pre>';
                                    // print_r($program);
                                    // die();
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                  <!-- <select class="select2" name="ogrencia{{$key}}[]" style="width:90%;">
                                    {{$program}}
                                  </select> -->
                                </td>
                                <td class="pull-right" style="padding-top:5px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td style="padding-top:18px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'pazartesi','b',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                  <!-- <select class="select2" name="ogrencib{{$key}}[]" style="width:90%;">
                                    {{$program}}
                                  </select> -->
                                </td>
                                <td class="pull-right" style="padding-top:18px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/sali">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td class="a_bes">a</td>
                              </tr>
                              <tr>
                                <td class="b_onsekiz">b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <tr>
                                <td style="padding-top:5px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'sali','a',$id,($key+1),$servis_id);
                                    $veriler = $sinif->name.',sali,a,'.$id.','.($key+1);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:5px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td style="padding-top:18px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'sali','b',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:18px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/carsamba">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td class="a_bes">a</td>
                              </tr>
                              <tr>
                                <td class="b_onsekiz">b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <tr>
                                <td style="padding-top:5px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'carsamba','a',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:5px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td style="padding-top:18px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'carsamba','b',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:18px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/persembe">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td class="a_bes">a</td>
                              </tr>
                              <tr>
                                <td class="b_onsekiz">b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <tr>
                                <td style="padding-top:5px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'persembe','a',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:5px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                              <tr>
                                <td style="padding-top:18px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'persembe','b',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:18px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                            </table>
                          </td>
                          <input type="hidden" name="sinif{{$key}}[]" value="{{$sinif->name}}/cuma">
                          <td>{{$sinif->name}}</td>
                          <td>
                            <table>
                              <tr>
                                <td class="a_bes">a</td>
                              </tr>
                              <tr>
                                <td class="b_onsekiz">b</td>
                              </tr>
                            </table>
                          </td>
                          <td>
                            <table style="width:100%;text-align:center;">
                              <tr>
                                <td style="padding-top:5px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'cuma','a',$id,($key+1),$servis_id);
                                    $veriler = $sinif->name.',cuma,a,'.$id.','.($key+1);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:5px;">
                                @if($program)
                                  {{$program->servis->name}}
                                @endif
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td style="padding-top:18px;">
                                  <?php
                                    $program = App\Helpers\helper::programFilter($sinif->name,'cuma','b',$id,($key+1),$servis_id);
                                  ?>
                                  @if($program)
                                  <span>{{$program->ogrenci->ad.' '.$program->ogrenci->soyad}}</span>
                                  @endif
                                </td>
                                <td class="pull-right" style="padding-top:18px;">
                                @if($program)
                                  {{$program->servis->name}}
                                  @endif
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        @endforeach
                        @endforeach
                      </table>
                      </form>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <div class="modal fade" id="modalOgrenciGuncelle" role="dialog">

    </div> <!--/.modal -->
@endsection
@section('edit-js')
<script>
setTimeout(function(){
  $('.sidebar-toggle').click(),1000
});
$(document).ready(function () {

  $('.a_bes').attr('style','padding-top:5px');
  $('.b_onsekiz').attr('style','padding-top:18px');
    $('.OgrenciGuncelle').click(function () {
        var ogrenci = $(this).attr('id');
        var servis_id = '{{$servis_id}}';
        var engel_kategorisi = '{{$engel_kategorisi}}';
        //console.log(engel_id);
        $.ajax({
            url: '/admin/program/ogrenci-modal',
            type: 'POST',
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            cache: false,
            data: {ogrenci: ogrenci,servis_id:servis_id,engel_kategorisi:engel_kategorisi},
            success: function(data){
                document.getElementById('modalOgrenciGuncelle').innerHTML=data;
                $('.select2').select2();
            },
            error: function(jqXHR, textStatus, err){}
        });
    });
});
</script>
@endsection
