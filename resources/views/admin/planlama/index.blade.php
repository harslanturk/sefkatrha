@extends('admin.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tüm Planlamalar
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{URL::to('/admin')}}"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
                <li class="active">Planlama</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <div>
                      @if(App\Helpers\helper::authControl('planlama','add'))
                      <a href="#" id="createCalendar" class="btn btn-block btn-primary btn-lg"  data-toggle="modal" data-target="#modalYeniPlanlama" style="margin-bottom:20px;">Yeni Planlama Ekle</a>
                      @endif
                    </div>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Planlamalarım</h3>
                            <div class="box-tools">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                              @foreach($calendars as $calendar)
                                <li style="background-color:#F7F7F7;">
                                  <b>{{\App\Helpers\Helper::dmYHi($calendar->start_time,'d M Y')}}</b>
                                </li>
                                <li style="background-color:#FFF;">
                                  <a href="#" id="{{$calendar->id}}" class="calendarModal"  data-toggle="modal" data-target="#modalUpdateCalendar">{{\App\Helpers\Helper::dmYHi($calendar->start_time,'H:i').' '.$calendar->title}}</a>
                                </li>
                              @endforeach
                            </ul>
                        </div><!-- /.box-body -->
                    </div><!-- /. box -->
                </div><!-- /.col -->
                <div class="col-md-9">
                    <div class="box box-primary">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <!-- Randevu Ekleme Modeli -->
    <div class="modal fade" id="modalYeniPlanlama" role="dialog">

    </div>
    <!-- /.modal -->
    <!-- Randevu Güncelleme Modeli -->
    <div class="modal fade" id="modalUpdateCalendar" role="dialog">

    </div>
    <!-- /.modal -->
@endsection
@section('edit-js')
    <script>
        $(document).ready(function() {

            //color picker with addon
            var calendar_url = "{{url('/admin/planlama')}}";
            if(calendar_url){
                var a =$('#calendar div > div');
                a[0].removeAttribute('style');
                //console.log(a[0]);
            }
            // function functionName() {
            //
            // }
            var base_url = '{{ url('/') }}';
            $('#calendar').fullCalendar({
                weekends: true,
                lang:'tr',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'agendaWeek,agendaDay'
                },
                defaultView: 'agendaWeek',
                editable: true,
                // Sürükleyerek Güncelleme Yapma
                eventDrop: function(event, delta) {
                    var start = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
                    var end = moment(event.end).format('YYYY-MM-DD HH:mm:ss');
                    //sweetAlert(event);
                    $.ajax({
                        url: '/admin/planlama/updateEventDrop',
                        type: 'POST',
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        cache: false,
                        data: {title:event.ad,
                            start_time:start,
                            end_time:end,
                            id:event.id},
                        success: function(data){
                          if (data == 1) {
                            sweetAlert('Güncelleme Başarılı.');
                          } else {
                              sweetAlert('Hata !');
                              setTimeout(function () {
                                  window.location.reload();
                              },1000);
                          }
                            //document.getElementById('modalYeniRandevu').innerHTML=data;
                        },
                        error: function(jqXHR, textStatus, err){}
                    });
                },
                dayClick: function(date, jsEvent, view) {
                    var start = date.format();
                    console.log(start.substr(11,5));
                    $('#createCalendar').click();
                    setTimeout(function () {
                        document.getElementsByName('start_date')[0].value = start.substr(0,10);
                        document.getElementsByName('start_time')[0].value = start.substr(11,8);
                        document.getElementsByName('end_date')[0].value = start.substr(0,10);
                        document.getElementsByName('end_time')[0].focus();
                    },700);
                    //sweetAlert(start);
                    /*alert('Clicked on: ' + date.format());

                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                    alert('Current view: ' + view.name);

                    // change the day's background color just for fun
                    $(this).css('background-color', 'red');*/

                },
                eventLimit: true, // allow "more" link when too many events
                events: {
                    url: base_url + '/admin/planlama/api',
                    error: function() {
                        sweetAlert("Gelen Json Verisi Bulunamadı.");
                    }
                }
            });
        });
        $('#createCalendar').click(function () {
            var deger = 'a';
            /*console.log($(this).attr('name'));*/
            $.ajax({
                url: '/admin/planlama/createCalendarModalShow',
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {deger: deger},
                success: function(data){
                    document.getElementById('modalYeniPlanlama').innerHTML=data;
                    //RenkSec(10,'color');
                    $('.select2').select2();
                },
                error: function(jqXHR, textStatus, err){}
            });
        });
        $('.calendarModal').click(function () {
            var event_id = $(this).attr('id');
            console.log(event_id);
            $.ajax({
                url: '/admin/planlama/editModal',
                type: 'POST',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                cache: false,
                data: {event_id: event_id},
                success: function(data){
                    document.getElementById('modalUpdateCalendar').innerHTML=data;
                    $('.select2').select2();
                },
                error: function(jqXHR, textStatus, err){}
            });
        });
        // function titleBtn() {
        //     console.log('asd');
        //     var title = $('#title').val().trim();
        //     if(title.length >= 5){
        //         document.getElementById('randevuSaveBtn').removeAttribute('disabled');
        //     }
        // }

        // function RenkSec(number,name) {
        //     var rand = Math.round((Math.random() * number));
        //     var renkler = ['lightblue','lightcoral','lightgrey','lightgreen','lightpink','lightsalmon','lightseagreen','lightskyblue','lightslategray','lightsteelblue'];
        //     console.log(renkler[rand]);
        //     document.getElementsByName(name)[0].value = renkler[rand];
        //     document.getElementsByName(name)[0].style.backgroundColor = renkler[rand];
        //     document.getElementsByName(name)[0].style.color = 'white';
        // }

    </script>
@endsection
