<div class="modal-dialog" role="document">
    <form role="form" action="{{URL::to('/admin/planlama/updateCalendar/'.$events->id)}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{$events->user_id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Planlama Güncelle</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group">
                        <label for="start_date">Öğretmen Seç</label>
                        <select class="select2" name="personel_id" style="width:100%;" required>
                          @foreach($personel as $val)
                          @if($events->personel_id == $val->id)
                          <option value="{{$val->id}}" selected>{{$val->name}}</option>
                          @else
                          <option value="{{$val->id}}">{{$val->name}}</option>
                          @endif
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Öğrenci Seç</label>
                        <select class="select2" name="ogrenci_id" style="width:100%;" required>
                          @foreach($ogrenci as $val)
                          @if($events->ogrenci_id == $val->id)
                          <option value="{{$val->id}}" selected>{{$val->ad}} {{$val->soyad}}</option>
                          @else
                          <option value="{{$val->id}}">{{$val->ad}} {{$val->soyad}}</option>
                          @endif
                          @endforeach
                        </select>
                    </div>
                      <div class="col-md-6" style="padding:0px;">
                          <label for="start_date">Sınıf Seç</label>
                          <select class="select2" name="sinif" style="width:98%;" required>
                            <option value="Z1"<?php if ($events->sinif == 'Z1') { echo "selected";} ?>>Z-1</option>
                            <option value="Z2"<?php if ($events->sinif == 'Z2') { echo "selected";} ?>>Z-2</option>
                            <option value="Z3"<?php if ($events->sinif == 'Z3') { echo "selected";} ?>>Z-3</option>
                            <option value="Z4"<?php if ($events->sinif == 'Z4') { echo "selected";} ?>>Z-4</option>
                            <option value="Z5"<?php if ($events->sinif == 'Z5') { echo "selected";} ?>>Z-5</option>
                            <option value="Z6"<?php if ($events->sinif == 'Z6') { echo "selected";} ?>>Z-6</option>
                            <option value="Z7"<?php if ($events->sinif == 'Z7') { echo "selected";} ?>>Z-7</option>
                            <option value="Z8"<?php if ($events->sinif == 'Z8') { echo "selected";} ?>>Z-8</option>
                            <option value="Z9"<?php if ($events->sinif == 'Z9') { echo "selected";} ?>>Z-9</option>
                            <option value="F1"<?php if ($events->sinif == 'F1') { echo "selected";} ?>>F-1</option>
                            <option value="F2"<?php if ($events->sinif == 'F2') { echo "selected";} ?>>F-2</option>
                            <option value="F3"<?php if ($events->sinif == 'F3') { echo "selected";} ?>>F-3</option>
                            <option value="F4"<?php if ($events->sinif == 'F4') { echo "selected";} ?>>F-4</option>
                          </select>
                      </div>
                      <div class="col-sm-6" style="padding:0px;">
                          <label for="start_date">Servis Seç</label>
                          <select class="select2" name="servis" style="width:98%;" required>
                            <option value="servis1"<?php if ($events->servis == 'servis1') { echo "selected";} ?>>Servis - 1</option>
                            <option value="servis2"<?php if ($events->servis == 'servis2') { echo "selected";} ?>>Servis - 2</option>
                            <option value="servis3"<?php if ($events->servis == 'servis3') { echo "selected";} ?>>Servis - 3</option>
                            <option value="servis4"<?php if ($events->servis == 'servis4') { echo "selected";} ?>>Servis - 4</option>
                          </select>
                      </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="start_date">Başlangıç Tarihi</label>
                        <input class="form-control" type="date" name="start_date" value="{{App\Helpers\helper::dmYHi($events->start_time,'Y-m-d')}}">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="start_date">Başlangıç Saati</label>
                        <input class="form-control" type="time" name="start_time" value="{{App\Helpers\helper::dmYHi($events->start_time,'H:i')}}">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Bitiş Tarihi</label>
                        <input class="form-control" type="date" name="end_date" value="{{App\Helpers\helper::dmYHi($events->end_time,'Y-m-d')}}">
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        <label for="finish_date">Bitiş Saati</label>
                        <input class="form-control" type="time" name="end_time" value="{{App\Helpers\helper::dmYHi($events->end_time,'H:i')}}">
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              @if(App\Helpers\helper::authControl('planlama','delete'))
                <a href="#" onclick="deleteApprove('/admin/planlama/delete/{{$events->id}}')" class="btn btn-danger"><i class="fa fa-trash"> Sil</i></a>
              @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
              @if(App\Helpers\helper::authControl('planlama','update'))
                <button id="randevuSaveBtn" type="submit" class="btn btn-primary">Planlama Güncelle</button>
              @endif
            </div>
        </div><!-- /.modal-content -->
    </form>
</div><!-- /.modal-dialog -->
