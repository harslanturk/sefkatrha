<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIslemDetayListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('islem_detay_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ogrenci_id');
            $table->integer('konu_id');
            $table->integer('gonderen_id');
            $table->integer('alici_id');
            $table->string('islem');
            $table->integer('status');
            $table->string('aciklama');
            $table->integer('issend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('islem_detay_list');
    }
}
