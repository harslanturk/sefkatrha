<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('yetki');
            $table->integer('tcno');
            $table->string('ad');
            $table->string('soyad');
            $table->string('cinsiyet');
            $table->date('dogum_tarihi');
            $table->string('dogum_yeri');
            $table->string('il');
            $table->string('ilce');
            $table->string('adres');
            $table->string('baba');
            $table->string('anne');
            $table->date('kayit_tarihi');
            $table->date('ayrilis_tarihi');
            $table->string('telefon');
            $table->string('is_telefon');
            $table->string('diger_telefon');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personel');
    }
}
