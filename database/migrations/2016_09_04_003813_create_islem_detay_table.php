<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIslemDetayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('islem_detay', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ogrenci_id');
            $table->string('konu');
            $table->integer('gonderen_id');
            $table->integer('alici_id');
            $table->string('islem');
            $table->string('aciklama');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('islem_detay');
    }
}
