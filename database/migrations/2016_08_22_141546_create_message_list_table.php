<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_id');
            $table->integer('gonderen_id');
            $table->integer('alici_id');
            $table->integer('read');
            $table->string('mesaj');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message_list');
    }
}
