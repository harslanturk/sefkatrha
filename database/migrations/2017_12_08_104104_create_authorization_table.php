<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorization',function(Blueprint $table){
        $table->increments('id');
        $table->integer('user_id');
        $table->integer('modul_id');
        $table->integer('group_id');
        $table->integer('read');
        $table->integer('update');
        $table->integer('delete');
        $table->integer('add');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
