<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('yetki');
            $table->string('tcno');
            $table->string('ad');
            $table->string('soyad');
            $table->string('cinsiyet');
            $table->date('dogum_tarihi');
            $table->string('dogum_yeri');
            $table->string('il');
            $table->string('ilce');
            $table->string('adres');
            $table->string('baba');
            $table->string('anne');
            $table->date('kayit_tarihi');
            $table->date('ayrilis_tarihi');
            $table->string('telefon');
            $table->string('is_telefon');
            $table->string('diger_telefon');
            $table->string('status');
            $table->string('color');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
