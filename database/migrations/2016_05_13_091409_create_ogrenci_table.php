<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOgrenciTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ogrenci', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ogrenci_no');
            $table->string('tcno');
            $table->string('ad');
            $table->string('soyad');
            $table->string('cinsiyet');
            $table->date('dogum_tarihi');
            $table->string('dogum_yeri');
            $table->string('engel_tipi');
            $table->string('il');
            $table->string('ilce');
            $table->string('adres');
            $table->string('baba');
            $table->string('anne');
            $table->date('kayit_tarihi');
            $table->date('ayrilis_tarihi');
            $table->string('okul');
            $table->string('egitim');
            $table->string('baba_tel');
            $table->string('anne_tel');
            $table->string('diger_tel');
            $table->string('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ogrenci');
    }
}
